# Aevol - An in silico experimental evolution platform

Aevol is a digital genetics model: populations of digital organisms are subjected to a process of
selection and variation, which creates a Darwinian dynamics.

By modifying the characteristics of selection (e.g. population size, type of environment,
environmental variations) or variation (e.g. mutation rates, chromosomal rearrangement rates, types
of rearrangements, horizontal transfer), one can study experimentally the impact of these parameters
on the structure of the evolved organisms. In particular, since Aevol integrates a precise and
realistic model of the genome, it allows for the study of structural variations of the genome (e.g.
number of genes, synteny, proportion of coding sequences).

The simulation platform comes along with a set of tools for analysing phylogenies and measuring many
characteristics of the organisms and populations along evolution.

## Pre-requisit
Here are the basic dependencies on a Debian system:
build-essential
cmake
zlib1g-dev
One option to install them all at once is to run the following command:
```shell
sudo apt install build-essential cmake zlib1g-dev
```

## Hot to compile?
```shell
mkdir build
cmake ..
make
```

This command will compile 4 executables: 
+ module_create
+ module_run
+ module_lineage
+ module_anc_stat

## How to run an expriment?
To run an example, e.g. basic, assuming you're in Aevol root directory:
```shell
  cd examples
  ../build/bin/module_create basic.json #1
  ../build/bin/module_run -b 0 -e 100 result.json #2
  ../build/bin/module_lineage -I 0 -e 100 result.json #3
  mkdir stats
  ../build/bin/module_anc_stat -Mv -j result.json lineage-b000000000-e000000100-i0.ae #4
```

Command by command:
1. It will create a new file `result.json` that will contain the genome of a new individual created using the parameters included in the `basic.json` file. The new file will also contain all the experimental parameters contained in `basic.json`.
2. It will run a simulation of evolution from generation 0 to 100 using a clonal population with the genome contained inside `result.json`. It will produce files inside `tree` directory corresponding of the phylogenetic tree of the simulation.
3. It will extract one lineage of the simulation using the data of the tree files. The lineage ends to the individual at location 0 on the simulation. It stores the lineage inside the file `lineage-b000000000-e000000100-i0.ae`.
4. From the initial genome of the simulation, it will compute information of all individuals of the lineage, replication after replication. It will store the information inside files in `stats/ancestor_stats/`.

visit www.aevol.fr for more information.
