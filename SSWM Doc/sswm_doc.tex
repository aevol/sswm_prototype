\documentclass[12pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[top=2cm, bottom=2cm, left=2cm, right=2cm]{geometry}
\usepackage{setspace}
\usepackage{fancyhdr}
\usepackage{graphicx}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage{array,multirow,makecell}
\usepackage[dvipsnames]{xcolor}
\usepackage{amsmath}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{enumitem}
\usepackage{listings}
\usepackage{filecontents}
\usepackage{verbatim}
\usepackage{eurosym}
\usepackage[export]{adjustbox}
\usepackage{ulem}
\usepackage{marginnote}
\usepackage{listings}

\usepackage{color}
\definecolor{deepblue}{rgb}{0,0,0.5}
\definecolor{deepred}{rgb}{0.6,0,0}
\definecolor{deepgreen}{rgb}{0,0.5,0}

\setcellgapes{1pt}
\makegapedcells
\pagestyle{fancy}
\newcolumntype{R}[1]{>{\raggedleft\arraybackslash}b{#1}}
\newcolumntype{L}[1]{>{\raggedright\arraybackslash }b{#1}}
\newcolumntype{C}[1]{>{\centering\arraybackslash }b{#1}}

\setstretch{1,5}
\renewcommand\headrulewidth{1pt}
\renewcommand{\thefootnote}{\alph{footnote}}

\lhead[L]{Yanis SINDT-BARET}
\rhead[R]{Beagle - Aevol}

\title{Documentation for Strong Selection Weak Mutations}
\author{Yanis SINDT-BARET}

\begin{document}
\begin{titlepage}

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}} 
\center 
\textsc{\LARGE INRIA
\\ Beagle - AEVOL}
\\ 2022
\vspace*{\stretch{1}}\\
\HRule \\[0.4cm]
{ \huge \bfseries Documentation for Strong Selection Weak Mutations}\\
\HRule \\[1cm]
\begin{minipage}{0.4\textwidth}
\begin{flushleft} \large
\emph{Done by : }\\
Yanis SINDT-BARET
\\
\end{flushleft}
\end{minipage}
~
\begin{minipage}{0.4\textwidth}
\begin{flushright} \large
\emph{Supervised by :} \\
Leonardo Trujillo\\
Paul Banse \\
\end{flushright}
\end{minipage}\\[0.4cm]
\selectfont
\vspace*{\stretch{1}}
\end{titlepage}

\lstset{
language=bash,
basicstyle=\ttm,
morekeywords={self, python3},              % Add keywords here
keywordstyle=\ttb\color{deepblue},
emph={MyClass,__init__},          % Custom highlighting
emphstyle=\ttb\color{deepred},    % Custom highlighting style
stringstyle=\color{deepgreen},
frame=tb,                         % Any extra options here
showstringspaces=false
}

\color{black}
\section{\underline{Compute simulation}}
\subsection{Pre-requisit}

\textbf{First}, you have to install Debian dependencies ; build-essential, cmake and zlib1g-dev. You can install it with this command :

\begin{lstlisting}[language=bash,caption={Install Pre-requisit}]
    sudo apt install build-essential cmake zlib1g-dev
\end{lstlisting}

\subsection{Compiling}
\textbf{Then} you have to compile all necesseries files :

\begin{lstlisting}[language=bash,caption={Compiling}]
    mkdir build #Not necessary if there is already a directory build
    cd build 
    cmake .. -DCMAKE_BUILD_TYPE=Release
    make
\end{lstlisting}

\subsection{Run simulation}

\subsubsection{Compute data for one hike}

If you are in the root directory, you have to go to the directory "examples" to have a \textit{basic.json} file (containing primordial information). If you need to generate a \textbf{random genome} (but viable) given in a result.json file, you have to do :

\begin{lstlisting}[language=bash,caption={Create a result.json file}]
    cd examples
    ../build/bin/module_create basic.json 
\end{lstlisting}

And then, to make a run with this \textit{result.json} file : 

\begin{lstlisting}[language=bash,caption={Do a single run}]
    cd examples
    ../build/bin/sswm result.json 0.5 50000 y
\end{lstlisting}

\textbf{With} \textbf{0.5} the probability of inversion (between 0 and 1). \textbf{50000} the number of generation. And \textbf{y} to allow neutral mutation (n to not allow it). So :
\begin{lstlisting}[language=bash,caption={Single run template}]
.../sswm NAME_RESULT_FILE PROBABILITY_INVERSION NUMBER_OF_GENERATION 
ALLOW_NEUTRAL_MUTATION
\end{lstlisting}

The program give a .csv output file, and its name follow this structure : 
"\textit{sswm\_evol\_result\_\{NB\_
GENERATION\}\{RESULT\_FILE\_NAME\}\_\{INVERSION\_RATE\}\_\{IS\_NEUTRAL\_MUTATION\_
ALLOWED\}\_\{HIKES\}\_.csv}". 

There are 6 columns

\begin{itemize}
\item nb\_gen : Current number of generation
\item fitness\_kept : Fitness selected by the model
\item new\_fitness : New fitness calculated with the new sequence mutated
\item mutation : Type of mutation (0 : Switch, 6 : Inversion)
\item nb\_prot\_kept : Number of protein viable with the sequence selected
\item new\_nb\_prot : New number of protein viable calculated with the new sequence mutated
\end{itemize}


It give also a json file for the last generation containing the DNA sequence. Follow the same name structure but \{RESULT\_FILE\_NAME\}\ is replace by \{JSON\_FILE\_NAME\}\

\subsubsection{Compute data for multiple hike}

If you want to do multiple Hikes, you should use a Python script \textbf{"\textit{multiple\_run.py}"}. So you need to run it, in a bash terminal (admit you are still in example) :

\begin{lstlisting}[language=bash,caption={Single run template}]
mkdir new_test_dir
cd new_test_dir
mkdir image
python3 multiple_run.py
\end{lstlisting}

\noindent The program wait from you few commit easily understandable (The number of Hike, Inversion Rate, the number of Generation) but for more information read these point : 

\begin{itemize}[label=\textbullet]
\item Create all basic files : When you want to run multiple hikes, the program create different basic files from one given. Write "n" only if the good number of basic files already exist.
\item Inversion rate : A number between 0 and 1 for the probability to do a inversion. (Switch rate is 1 - Inversion rate)
\item Number of generation : The number of generation (Usually 5.000.000)
\item If you want to not allow neutral mutation, you have to uncomment line 55 in \textbf{"multiple\_run.py"}
\item If you want to setup the same genome for all hikes, uncomment line 17 in \textbf{"multiple\_run.py"}
\end{itemize}

\noindent This will give you in your new\_test\_dir all \textit{basic.json} files, \textit{resultat.json} files and all \textit{csv} files.
 
\noindent If you want to try different setting (usually different probability), you should do 30 hikes (as example) for a probability chosen on a bash terminal, and then the same number of hikes with an other probability on a other bash terminal.


\end{document}


