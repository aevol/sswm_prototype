import json
import subprocess
import os
import random


def clone_basic_file(index: int, path: str) -> None:
    """Function to clone a basic file with a new seed.

    Args:
        index (int): Index of the hike
        path (str): Path to a basic file to clone
    """
    with open(path, "r") as file:
        data_json = json.load(file)
    data_json["seed"] = random.randint(1, 1000000)
    #    data_json["seed"] = 5 ## If you want to set up a genome for all hikes.
    old_path_splited = path.split(".json")
    new_path = f"{old_path_splited[0]}_{index}.json"
    with open(new_path, "w") as result_file:
        json.dump(data_json, result_file)


def change_resultjson_seed(index: int, result_id: str, path="") -> None:
    """Function to clone a result.json file (so a DNA sequence) with a new seed.

    Args:
        index (int): Index of the hike
        result_id (str): ID chosen
        path (str, optional): Path to the result file, if not located in the working directory. Defaults to "".
    """
    with open(f"{path}result.json", "r") as file:
        data_json = json.load(file)
    data_json["seed"] = random.randint(1, 1000000)
    with open(f"{path}result_{result_id}_{index}.json", "w") as result_file:
        json.dump(data_json, result_file)


if __name__ == "__main__":
    """Program to do multiple experiment.
    It takes a basic_file to clone it in X times (corresponding to X different sequence)
    And do X hikes with these differents genomes.
    It creates 4 differents type of file :
        basic_{hike}.json : basic file that give some basic necessary information like the seed to create the gene
        result_{ID}_{hike}.json : result file which content the sequence with a new seed to do mutations.
        sswm_evol_result_{number of generation}{result file name linked}_{probability of inversion}_{y if it allow neutral mutation else n}_{hikes}.csv :
            csv file with 6 columns : | Generation | Fitness kept | Fitness after mutation | Type of mutation at this generation | Number of protein kept | Number of protein after mutation |
    """

    load_basic: bool = input("Do we need to create all basic files ? (y/n) : ")
    nb_hikes = int(
        input("How many hikes do you want to do ? : ")
    )  # Todo: Try exept if wrong input
    inversion_rate = input("Which probability of inversion ? : ")
    nb_gen = int(input("How many generation ? : "))
    is_neutral_mut = "y"
    #    is_neutral_mut = input("Do you allow neutral mutation ? (y/n) : ").lower()  ## Uncomment it to let user to choose it and comment the following line
    temp = str(inversion_rate).split(".")
    result_id: str = temp[0] + "00" if len(temp) == 1 else temp[0] + temp[1] + "0"
    os.chdir(os.getcwd())
    random.seed(33)

    if load_basic == "y":
        for i in range(1, nb_hikes + 1):
            clone_basic_file(i, "basic.json")
    for i in range(1, nb_hikes + 1):
        res_a = subprocess.run(
            args=["../../build/bin/module_create", f"basic_{i}.json"],
            stdout=subprocess.DEVNULL,
        )

        change_resultjson_seed(i, result_id)
        res_b = subprocess.run(
            args=[
                "../../build/bin/sswm",
                f"result_{result_id}_{i}.json",
                str(inversion_rate),
                str(nb_gen),
                str(is_neutral_mut),
                str(i),
            ]
        )

        if res_b.returncode == 0:
            print("\n --- \n Le programme s'est exécuté sans erreur")
        else:
            print("\n --- \n Une erreur est survenue")

        print(f"\n{i}")
