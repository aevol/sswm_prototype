//
// Created by elturpin on 30/05/22.
//

#include "Translator.h"

#include "Individual_7.h"
#include "Metadata.h"
#include "macros.h"

#include <fstream>

namespace aevol {
void translate_protein_2B(Protein_7& prot, const Dna_7& dna_, double w_max) {
  auto dna_length = dna_.length();
  if (prot.is_init_) {
    int c_pos = prot.protein_start, t_pos;
    if (prot.leading_lagging == 0) {
      c_pos = c_pos >= dna_length ? c_pos - dna_length : c_pos;
    } else {
      c_pos = c_pos < 0 ? dna_length + c_pos : c_pos;
    }

    int8_t value = 0;
    int16_t codon_idx = 0;
    int32_t count_loop = 0;

    if (prot.leading_lagging == 0) {
      // LEADING
      while (count_loop < prot.protein_length && codon_idx < 64 * 3) {
        value = 0;
        for (int8_t i = 0; i < 3; i++) {
          t_pos = c_pos + i;
          if (dna_.get_lead(t_pos) == '1')
            value += 1 << (CODON_SIZE - i - 1);
        }

        prot.codon_list.push_back(value);

        codon_idx++;

        count_loop++;
        c_pos += CODON_SIZE;
        c_pos = c_pos >= dna_length ? c_pos - dna_length : c_pos;
      }
    } else {
      // LAGGING
      while (count_loop < prot.protein_length && codon_idx < 64 * 3) {
        value = 0;
        for (int8_t i = 0; i < 3; i++) {
          t_pos = c_pos - i;
          if (dna_.get_lag(t_pos) != '1')
            value += 1 << (CODON_SIZE - i - 1);
        }

        prot.codon_list.push_back(value);

        codon_idx++;

        count_loop++;

        c_pos -= CODON_SIZE;
        c_pos = c_pos < 0 ? c_pos + dna_length : c_pos;
      }
    }

    if (codon_idx >= 64 * 3) {
      std::ofstream last_gener_file("aevol_run.log", std::ofstream::out);
      last_gener_file
          << "Stop translating protein before end (length is greater than 196"
          << std::endl;
      std::cout
          << "Stop translating protein before end (length is greater than 196"
          << std::endl;
      last_gener_file.close();
    }

    double M = 0.0;
    double W = 0.0;
    double H = 0.0;

    int32_t nb_m = 0;
    int32_t nb_w = 0;
    int32_t nb_h = 0;

    bool bin_m =
        false;  // Initializing to false will yield a conservation of the high weight bit
    bool bin_w =
        false;  // when applying the XOR operator for the Gray to standard conversion
    bool bin_h = false;

    prot.nb_codons_ = codon_idx - 1;

    for (int i = 0; i < codon_idx; i++) {
      switch (prot.codon_list[i]) {
      case CODON_M0: {
        // M codon found
        nb_m++;

        // Convert Gray code to "standard" binary code
        bin_m ^=
            false;  // as bin_m was initialized to false, the XOR will have no effect on the high weight bit

        // A lower-than-the-previous-lowest weight bit was found, make a left bitwise shift
        //~ M <<= 1;
        M *= 2;

        // Add this nucleotide's contribution to M
        if (bin_m)
          M += 1;

        break;
      }
      case CODON_M1: {
        // M codon found
        nb_m++;

        // Convert Gray code to "standard" binary code
        bin_m ^=
            true;  // as bin_m was initialized to false, the XOR will have no effect on the high weight bit

        // A lower-than-the-previous-lowest bit was found, make a left bitwise shift
        //~ M <<= 1;
        M *= 2;

        // Add this nucleotide's contribution to M
        if (bin_m)
          M += 1;

        break;
      }
      case CODON_W0: {
        // W codon found
        nb_w++;

        // Convert Gray code to "standard" binary code
        bin_w ^=
            false;  // as bin_m was initialized to false, the XOR will have no effect on the high weight bit

        // A lower-than-the-previous-lowest weight bit was found, make a left bitwise shift
        //~ W <<= 1;
        W *= 2;

        // Add this nucleotide's contribution to W
        if (bin_w)
          W += 1;

        break;
      }
      case CODON_W1: {
        // W codon found
        nb_w++;

        // Convert Gray code to "standard" binary code
        bin_w ^=
            true;  // as bin_m was initialized to false, the XOR will have no effect on the high weight bit

        // A lower-than-the-previous-lowest weight bit was found, make a left bitwise shift
        //~ W <<= 1;
        W *= 2;

        // Add this nucleotide's contribution to W
        if (bin_w)
          W += 1;

        break;
      }
      case CODON_H0:
      case CODON_START:  // Start codon codes for the same amino-acid as H0 codon
      {
        // H codon found
        nb_h++;

        // Convert Gray code to "standard" binary code
        bin_h ^=
            false;  // as bin_m was initialized to false, the XOR will have no effect on the high weight bit

        // A lower-than-the-previous-lowest weight bit was found, make a left bitwise shift
        //~ H <<= 1;
        H *= 2;

        // Add this nucleotide's contribution to H
        if (bin_h)
          H += 1;

        break;
      }
      case CODON_H1: {
        // H codon found
        nb_h++;

        // Convert Gray code to "standard" binary code
        bin_h ^=
            true;  // as bin_m was initialized to false, the XOR will have no effect on the high weight bit

        // A lower-than-the-previous-lowest weight bit was found, make a left bitwise shift
        //~ H <<= 1;
        H *= 2;

        // Add this nucleotide's contribution to H
        if (bin_h)
          H += 1;

        break;
      }
      }
    }

    //  ----------------------------------------------------------------------------------
    //  2) Normalize M, W and H values in [0;1] according to number of codons of each kind
    //  ----------------------------------------------------------------------------------

    prot.m = nb_m != 0 ? M / (pow(2, nb_m) - 1) : 0.5;
    prot.w = nb_w != 0 ? W / (pow(2, nb_w) - 1) : 0.0;
    prot.h = nb_h != 0 ? H / (pow(2, nb_h) - 1) : 0.5;
    //  ------------------------------------------------------------------------------------
    //  3) Normalize M, W and H values according to the allowed ranges (defined in macros.h)
    //  ------------------------------------------------------------------------------------
    // x_min <= M <= x_max
    // w_min <= W <= w_max
    // h_min <= H <= h_max
    prot.m = (X_MAX - X_MIN) * prot.m + X_MIN;
    prot.w = (w_max - W_MIN) * prot.w + W_MIN;
    prot.h = (H_MAX - H_MIN) * prot.h + H_MIN;

    if (nb_m == 0 || nb_w == 0 || nb_h == 0 || prot.w == 0.0 || prot.h == 0.0) {
      prot.is_functional = false;
    } else {
      prot.is_functional = true;
    }
  }
}

void ProteinTranslator4B::operator()(Protein_7& prot,
                                     const Dna_7& dna_,
                                     double w_max) const {
  auto dna_length = dna_.length();
  if (prot.is_init_) {
    int c_pos = prot.protein_start;
    if (prot.leading_lagging == 0) {

      c_pos = c_pos >= dna_length ? c_pos - dna_length : c_pos;
    } else {
      c_pos = c_pos < 0 ? dna_length + c_pos : c_pos;
    }

    int8_t value;
    int16_t codon_idx = 0;
    int32_t count_loop = 0;

    char base_1;
    char base_2;
    char base_3;

    if (prot.leading_lagging == 0) {  // LEADING
      while (count_loop < prot.protein_length) {
        base_1 = dna_.get_lead(c_pos);
        base_2 = dna_.get_lead(c_pos + 1);
        base_3 = dna_.get_lead(c_pos + 2);

        value = bases_to_codon_value(base_1, base_2, base_3);
        prot.codon_list.push_back(value);

        codon_idx++;

        count_loop++;
        c_pos += CODON_SIZE;
        c_pos = c_pos >= dna_length ? c_pos - dna_length : c_pos;
      }
    } else {  // LAGGING
      while (count_loop < prot.protein_length) {

        // get complementary base on lagging strain
        base_1 = get_complementary_base(dna_.get_lead(c_pos));
        base_2 = get_complementary_base(dna_.get_lead(c_pos - 1));
        base_3 = get_complementary_base(dna_.get_lead(c_pos - 2));

        value = bases_to_codon_value(base_1, base_2, base_3);
        prot.codon_list.push_back(value);

        codon_idx++;

        count_loop++;

        c_pos -= CODON_SIZE;
        c_pos = c_pos < 0 ? c_pos + dna_length : c_pos;
      }
    }

    //  --------------------------------
    //  1) Compute values for M, W and H
    //  --------------------------------

    long double M = 0.0;
    long double W = 0.0;
    long double H = 0.0;

    prot.nb_codons_ = codon_idx;

    int32_t nb_m = 0;
    int32_t nb_w = 0;
    int32_t nb_h = 0;

    double m_digit_factor = 1.0;
    double w_digit_factor = 1.0;
    double h_digit_factor = 1.0;

    for (int i = 0; i < prot.nb_codons_; i++) {
      AminoAcid amino_acid =
          codon_value_to_aminoacid(prot.codon_list.at(prot.nb_codons_ - i - 1));

      if (base_m[amino_acid] != -1) {
        M += base_m[amino_acid] * m_digit_factor;
        m_digit_factor *= base_m_size;
        nb_m++;
      }

      if (base_w[amino_acid] != -1) {
        W += base_w[amino_acid] * w_digit_factor;
        w_digit_factor *= base_w_size;
        nb_w++;
      }

      if (base_h[amino_acid] != -1) {
        H += base_h[amino_acid] * h_digit_factor;
        h_digit_factor *= base_h_size;
        nb_h++;
      }
    }

    //  ----------------------------------------------------------------------------------
    //  2) Normalize M, W and H values in [0;1] according to number of codons of each kind
    //  ----------------------------------------------------------------------------------
    if (nb_m != 0) {
      prot.m = M / (pow(base_m_size, nb_m) - 1);
    } else {
      prot.m = 0.5;
    }

    if (nb_w != 0) {
      prot.w = W / (pow(base_w_size, nb_w) - 1);
    } else {
      prot.w = 0.0;
    }

    if (nb_h != 0) {
      prot.h = H / (pow(base_h_size, nb_h) - 1);
    } else {
      prot.h = 0.5;
    }
    //  ------------------------------------------------------------------------------------
    //  3) Normalize M, W and H values according to the allowed ranges (defined in macros.h)
    //  ------------------------------------------------------------------------------------
    // x_min <= M <= x_max
    // w_min <= W <= w_max
    // h_min <= H <= h_max
    prot.m = (X_MAX - X_MIN) * prot.m + X_MIN;
    prot.w = (w_max - W_MIN) * prot.w + W_MIN;
    prot.h = (H_MAX - H_MIN) * prot.h + H_MIN;

    if (nb_m == 0 || nb_w == 0 || nb_h == 0 || prot.w == 0.0 || prot.h == 0.0) {
      prot.is_functional = false;
    } else {
      prot.is_functional = true;
    }
  }
}
}