// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************


#include "Rna_7.h"
#include "ae_enums.h"
#include "Protein_7.h"
#include "Individual_7.h"
#include "ExpManager_7.h"
namespace aevol {

    Rna_7::Rna_7(int32_t t_begin,
        int32_t t_end,
        bool t_leading_lagging,
        double t_e,
        int32_t t_length,PromoterStruct* promo) {
    begin             = t_begin;
    end               = t_end;
    leading_lagging   = t_leading_lagging;
    e                 = t_e;
    length            = t_length;
    is_coding_        = false;
    is_init_          = true;
    start_prot_count_ = 0;
    to_compute_start_pos_ = true;
    prom = promo;
    promo->rna = this;
    //  start_prot = new std::list<int32_t>();
  }

Rna_7::Rna_7(Rna_7* clone,PromoterStruct* promo) {
    pos                = clone->pos;
    error              = clone->error;
    leading_or_lagging = clone->leading_or_lagging;
    begin             = clone->begin;
    end               = clone->end;
    leading_lagging   = clone->leading_lagging;
    e                 = clone->e;
    length            = clone->length;
    is_coding_        = clone->is_coding_;
    is_init_          = clone->is_init_;
    start_prot_count_ = clone->start_prot_count_;
    // start_prot = new std::list<int32_t>();
    // if (clone->start_prot != nullptr) {
      for (auto pos : (clone->start_prot))
        start_prot.push_back(pos);
    // }
    //to_compute_end_ =   clone->to_compute_end_;
    if (promo != nullptr)
      prom = promo;
    else
      prom = clone->prom;

    to_compute_start_pos_ = clone->to_compute_start_pos_;
    promo->rna = this;
  }
}