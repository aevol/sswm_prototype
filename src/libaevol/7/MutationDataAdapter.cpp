//
// Created by elturpin on 10/06/22.
//

#include "MutationDataAdapter.h"

#include "Deletion.h"
#include "Duplication.h"
#include "Inversion.h"
#include "PointMutation.h"
#include "SmallDeletion.h"
#include "SmallInsertion.h"
#include "Translocation.h"

using namespace aevol;

Mutation* MutationDataAdapter::mutation_event_to_mutation(
    const MutationEvent& mut_event) {
  Mutation* mut;
  switch (mut_event.type()) {
  case DO_SWITCH:
    mut = new PointMutation(mut_event.pos_1()
#ifdef BASE_4
    , mut_event.base()
#endif
    );
    break;
  case SMALL_INSERTION:
    mut = new SmallInsertion(mut_event.pos_1(), mut_event.number(),
                             mut_event.seq());

    break;
  case SMALL_DELETION:
    mut = new SmallDeletion(mut_event.pos_1(), mut_event.number());
    break;
  case DUPLICATION:
    mut = new Duplication(mut_event.pos_1(), mut_event.pos_2(),
                          mut_event.pos_3(), mut_event.number());
    break;
  case TRANSLOCATION:
    mut = new Translocation(mut_event.pos_1(), mut_event.pos_2(),
                            mut_event.pos_3(), mut_event.pos_4(),
                            mut_event.number(), mut_event.invert());
    break;
  case INVERSION:
    mut =
        new Inversion(mut_event.pos_1(), mut_event.pos_2(), mut_event.number());
    break;
  case DELETION:
    mut =
        new Deletion(mut_event.pos_1(), mut_event.pos_2(), mut_event.number());
    break;
  default:
    mut = nullptr;
  }
  return mut;
}

MutationEvent*
MutationDataAdapter::mutation_to_mutation_event(const Mutation& mut) {
  auto* mevent = new MutationEvent();
  switch (mut.mut_type()) {
  case SWITCH: {
    const auto& pmut = dynamic_cast<const PointMutation&>(mut);
#ifdef BASE_2
    mevent->switch_pos(pmut.pos());
#elif BASE_4
    mevent->switch_pos(pmut.pos(), pmut.base());
#endif
    break;
  }
  case S_INS: {
    const auto& s_ins = dynamic_cast<const SmallInsertion&>(mut);
    mevent->small_insertion(s_ins.pos(), s_ins.length(), s_ins.seq());
    break;
  }
  case S_DEL: {
    const auto& s_del = dynamic_cast<const SmallDeletion&>(mut);
    mevent->small_deletion(s_del.pos(), s_del.length());
    break;
  }
  case DUPL: {
    const auto& dupl = dynamic_cast<const Duplication&>(mut);
    mevent->duplication(dupl.pos1(), dupl.pos2(), dupl.pos3(), dupl.length());
    break;
  }
  case DEL: {
    const auto& del = dynamic_cast<const Deletion&>(mut);
    mevent->deletion(del.pos1(), del.pos2(), del.length());
    break;
  }
  case TRANS: {
    const auto& trans = dynamic_cast<const Translocation&>(mut);
    mevent->translocation(trans.pos1(), trans.pos2(), trans.pos3(),
                          trans.pos4(), trans.invert(), trans.length());
    break;
  }
  case INV: {
    const auto& inv = dynamic_cast<const Inversion&>(mut);
    mevent->inversion(inv.pos1(), inv.pos2(), inv.length());
    break;
  }
  default:
    fprintf(stderr, "ERROR, invalid mutation type in file %s:%d\n", __FILE__,
            __LINE__);
    exit(EXIT_FAILURE);
  }
  return mevent;
}