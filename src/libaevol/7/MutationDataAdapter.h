//
// Created by elturpin on 10/06/22.
//

#pragma once

#include "Mutation.h"
#include "MutationEvent.h"

namespace aevol {

namespace MutationDataAdapter {
  Mutation* mutation_event_to_mutation(const MutationEvent& mut_event);
  MutationEvent* mutation_to_mutation_event(const Mutation& mut);
};

}  // namespace aevol
