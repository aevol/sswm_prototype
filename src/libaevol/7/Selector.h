//
// Created by elturpin on 02/06/22.
//

#ifndef AEVOL_SELECTOR_H
#define AEVOL_SELECTOR_H

#include "Individual_7.h"
#include "JumpingMT.h"

#include <memory>
#include <vector>

namespace aevol {
using IndividualArray = std::vector<std::shared_ptr<Individual_7>>;
using UPrngArray = std::vector<std::unique_ptr<JumpingMT>>;

class Selector {
 public:
  virtual ~Selector() = default;
  virtual void
  operator()(const IndividualArray& population,
             std::vector<int>& reproducers_location,
             UPrngArray& prngs) const = 0;
};

class GlobalSelector: public Selector{
 private:
  int grid_size_;

 public:
  GlobalSelector(int grid_size) : grid_size_(grid_size) {}
  ~GlobalSelector() = default;

  void operator()(const IndividualArray& population,
                  std::vector<int>& reproducers_location,
                  UPrngArray& prngs) const override;
};

class LocalSelector: public Selector {
 private:
  int selection_scope_x_;
  int selection_scope_y_;
  int grid_height_;
  int grid_width_;

  void select_one_location(int location,
                           int& location_of_reproducer,
                           const IndividualArray& population,
                           JumpingMT& prng) const;

 public:
  LocalSelector(int scope_x, int scope_y, int grid_h, int grid_w)
      : selection_scope_x_(scope_x), selection_scope_y_(scope_y),
        grid_height_(grid_h), grid_width_(grid_w) {}
  ~LocalSelector() = default;

  void operator()(const IndividualArray& population,
                  std::vector<int>& reproducers_location,
                  UPrngArray& prngs) const override;
};
}
#endif  //AEVOL_SELECTOR_H
