// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons, Jonathan Rouzaud-Cornabas
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "FuzzyFactory_7.h"

#include "AbstractFuzzy_7.h"
#include "Discrete_Double_Fuzzy.h"
#include "Vector_Fuzzy.h"

#include <list>
namespace aevol {
FuzzyFactory_7::FuzzyFactory_7(int flavor, int sampling) {

  flavor_ = (FuzzyFactory_Flavor)flavor;
  if (flavor_ >= 2) {
    PHENOTYPE_VECTOR_SIZE = sampling;
    D_PHENOTYPE_VECTOR_SIZE = ((double)sampling);
  }
}

AbstractFuzzy_7* FuzzyFactory_7::create_fuzzy() {
  AbstractFuzzy_7* fuzz = nullptr;
  switch (flavor_) {
  case FuzzyFactory_Flavor::LIST:
    //fuzz = new List_Fuzzy();
    printf("List Fuzzy are currently not available\n");
    exit(-1);
    break;
  case FuzzyFactory_Flavor::VECTOR:
    fuzz = new Vector_Fuzzy();
    break;
  case FuzzyFactory_Flavor::DISCRETE_DOUBLE_TABLE:
    // printf("Size %d \n",PHENOTYPE_VECTOR_SIZE);
    fuzz = new Discrete_Double_Fuzzy(PHENOTYPE_VECTOR_SIZE);
    break;
  default:
    fuzz = new Vector_Fuzzy();
    break;
  }
  return fuzz;
}

}  // namespace aevol