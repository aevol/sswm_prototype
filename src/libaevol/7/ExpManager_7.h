// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************


#ifndef AEVOL_EXPMANAGER_7_H
#define AEVOL_EXPMANAGER_7_H

#include "FuzzyFactory_7.h"
#include "DnaMutator.h"
#include "Tree.h"
#include "Stats_7.h"

#include "Selector.h"

namespace aevol {

struct MutationParameters {
  // ------------------------------ Rearrangement rates (without alignements)
  double duplication_rate_;
  double deletion_rate_;
  double translocation_rate_;
  double inversion_rate_;

  // --------------------------------------------------------- Mutation rates
  double point_mutation_rate_;
  double small_insertion_rate_;
  double small_deletion_rate_;
  int16_t max_indel_size_;

  int32_t min_genome_length_;
  int32_t max_genome_length_;
};

class ExpManager_7 {
 public:
  ExpManager_7(double width,
               double height,
               int backup_step,
               int tree_step,
               MutationParameters mut_parameters,
               FuzzyFactory_7* factory,
               AbstractFuzzy_7* src_target,
               std::unique_ptr<Selector>&& selection);

  ~ExpManager_7();

  void run_a_step(double w_max, double selection_pressure);

// Function with ID
  void apply_mutations(int indiv_id);

  void do_mutation(int indiv_id);

  void write_stat(bool just_best = true, bool non_coding = false);

  std::vector<std::shared_ptr<Individual_7>> current_individuals;
  std::vector<std::shared_ptr<Individual_7>> previous_individuals;
  // Todo: Not updated each generation !!
  std::shared_ptr<Individual_7> best_indiv;

  std::vector<int> mutant_list_;

  int32_t nb_indivs_;
  int32_t nb_clones_;

  int rna_grain_size = 32;
  int protein_grain_size = 32;

  // static bool compute_diff_rnas;

  FuzzyFactory_7* fuzzy_factory_;

  double* fitness_sum_tab_;
  AbstractFuzzy_7* target;

  int32_t grid_width_;
  int32_t grid_height_;

  int32_t selection_scope_x_;
  int32_t selection_scope_y_;

  FitnessFunction fitness_function_;

  // DNA Mutator tables
  std::vector<DnaMutator*> dna_mutator_array_;
  MutationParameters mutation_parameters_;

  // Which individual will be placed in each GridCell
  std::vector<int> next_generation_reproducer_;

  std::vector<std::unique_ptr<JumpingMT>> reprod_prng_;
  std::vector<std::shared_ptr<JumpingMT>> mut_prng_;

  Tree* tree_ = nullptr;

  int backup_step_;

 private:
  Stats_7* stats_best = nullptr;
  Stats_7* stats_mean = nullptr;

  std::unique_ptr<Selector> selector;

  void selection(int indiv_id);

};
}

#endif //AEVOL_EXPMANAGER_7_H
