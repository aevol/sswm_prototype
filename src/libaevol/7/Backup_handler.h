//
// Created by elturpin on 18/05/22.
//

#pragma once

#include "Individual_7.h"
#include "JumpingMT.h"

#include <memory>

namespace aevol {

using IndividualArray = std::vector<std::shared_ptr<Individual_7>>;
using SPrngArray = std::vector<std::shared_ptr<JumpingMT>>;
using UPrngArray = std::vector<std::unique_ptr<JumpingMT>>;

namespace bu {
void write_backup(int generation,
                  const IndividualArray& population,
                  const UPrngArray& reprod_prngs,
                  const SPrngArray& mut_prngs);

IndividualArray read_backup(int generation,
                            IndividualArray& population,
                            UPrngArray& reprod_prngs,
                            SPrngArray& mut_prngs);
}
}
