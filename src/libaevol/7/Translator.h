//
// Created by elturpin on 01/06/22.
//

#ifndef AEVOL_TRANSLATOR_H
#define AEVOL_TRANSLATOR_H

#include "Protein_7.h"

namespace aevol {
void translate_protein_2B(Protein_7& prot, const Dna_7& dna_, double w_max);

class ProteinTranslator4B {
 private:
  int8_t base_m[NB_AMINO_ACIDS];
  int8_t base_w[NB_AMINO_ACIDS];
  int8_t base_h[NB_AMINO_ACIDS];

  int8_t base_m_size;
  int8_t base_w_size;
  int8_t base_h_size;

 public:
  constexpr ProteinTranslator4B(): base_m(), base_w(), base_h(),
  base_m_size(7), base_w_size(6), base_h_size(7){
    for(int8_t i = 0; i < NB_AMINO_ACIDS; i++) {
      base_m[i] = -1;
      base_w[i] = -1;
      base_h[i] = -1;
    }
    base_m[ARGININE] = 2;
    base_m[ASPARAGINE] = 4;
    base_m[ASPARTIC_ACID] = 5;
    base_m[GLUTAMIC_ACID] = 6;
    base_m[GLUTAMINE] = 0;
    base_m[HISTIDINE] = 1;
    base_m[SERINE] = 3;

    base_w[ALANINE] = 5;
    base_w[CYSTEINE] = 0;
    base_w[ISOLEUCINE] = 4;
    base_w[LEUCINE] = 3;
    base_w[THREONINE] = 1;
    base_w[PHENYLALANINE] = 2;

    base_h[GLYCINE] = 3;
    base_h[METHIONINE] = 2;
    base_h[LYSINE] = 5;
    base_h[PROLINE] = 6;
    base_h[TRYPTOPHAN] = 1;
    base_h[TYROSINE] = 4;
    base_h[VALINE] = 0;
  }

  void operator()(Protein_7& prot, const Dna_7& dna_, double w_max) const;
};

constexpr auto translate_protein_4B = ProteinTranslator4B();
}

// Todo: 4BASES
inline int8_t bases_to_codon_value(char b1, char b2, char b3) {
  return ((b1-'0') << 4) | ((b2-'0') << 2) | (b3-'0');
}

// Todo: 4BASES
inline AminoAcid codon_value_to_aminoacid(int8_t value) {
  char base_1 = '0' + ((value >> 4) & 3),
       base_2 = '0' + ((value >> 2) & 3),
       base_3 = '0' + (value & 3);

  if(base_2 == BASE_T)                                  // ** -T- **
  {
    if(base_1 == BASE_G) return VALINE;                   // GT-
    if(base_1 == BASE_C) return LEUCINE;                  // CT-

    if(base_1 == BASE_A) {
      if(base_3 == BASE_G) return METHIONINE;             // ATG
      else return ISOLEUCINE;                             // AT(T/C/A)
    }

    // base_1 == BASE_T
    if(base_3 == BASE_T || base_3 == BASE_C)
      return PHENYLALANINE;                               // TT(T/C)
    else
      return LEUCINE;                                     // TT(A/G)
  }
  else if(base_2 == BASE_C)                             // ** -C- **
  {
    switch(base_1)
    {
    case BASE_T:
      return SERINE;                                    // TC-

    case BASE_C:
      return PROLINE;                                   // CC-

    case BASE_A:
      return THREONINE;                                 // AC-

    case BASE_G:
      return ALANINE;                                   // GC-
    }
  }
  else if(base_2 == BASE_A)                             // ** -A- **
  {
    switch(base_1)
    {
    case BASE_T:
      if(base_3 == BASE_T || base_3 == BASE_C)
        return TYROSINE;                                // TA(T/C)
      else
        return STOP;                                    // TA(A/G)

    case BASE_C:
      if(base_3 == BASE_T || base_3 == BASE_C)
        return HISTIDINE;                               // CA(T/C)
      else
        return GLUTAMINE;                               // CA(A/G)

    case BASE_A:
      if(base_3 == BASE_T || base_3 == BASE_C)
        return ASPARAGINE;                              // AA(T/C)
      else
        return LYSINE;                                  // AA(A/G)

    case BASE_G:
      if(base_3 == BASE_T || base_3 == BASE_C)
        return ASPARTIC_ACID;                           // GA(T/C)
      else
        return GLUTAMIC_ACID;                           // GA(A/G)
    }
  }
  else if(base_2 == BASE_G)                             // ** -G- **
  {
    switch(base_1)
    {
    case BASE_T:
      if(base_3 == BASE_A) return STOP;                 // TGA
      if(base_3 == BASE_G) return TRYPTOPHAN;           // TGG

      return CYSTEINE;                                  // TG(T/C)

    case BASE_C:
      return ARGININE;                                  // CG-

    case BASE_A:
      if(base_3 == BASE_A || base_3 == BASE_G)
        return ARGININE;                                // CG(A/G)

      return SERINE;                                    // CG(T/C)

    case BASE_G:
      return GLYCINE;                                   // GG-
    }
  }
  exit(1);
}


#endif  //AEVOL_TRANSLATOR_H
