// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons, Jonathan Rouzaud-Cornabas
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_FUZZYFACTORY_H
#define AEVOL_FUZZYFACTORY_H

#include "AbstractFuzzy_7.h"

namespace aevol {
enum FuzzyFactory_Flavor {
  LIST = 0,
  VECTOR = 1,
  DISCRETE_FLOAT_TABLE = 2,
  DISCRETE_DOUBLE_TABLE = 3
};

class FuzzyFactory_7 {
 public:
  FuzzyFactory_7(int flavor, int sampling);

  AbstractFuzzy_7* create_fuzzy();

 private:
  FuzzyFactory_Flavor flavor_;

  int32_t PHENOTYPE_VECTOR_SIZE = -1;
  double D_PHENOTYPE_VECTOR_SIZE = ((double)PHENOTYPE_VECTOR_SIZE);
};

}  // namespace aevol
#endif  //AEVOL_DNAFACTORY_H
