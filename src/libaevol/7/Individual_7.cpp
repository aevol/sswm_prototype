// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "Individual_7.h"

#include "Metadata.h"
#include "Protein_7.h"
#include "Translator.h"

#include <algorithm>
#include <cstring>

namespace aevol {

/** Individual_7 Constructor and Destructor **/
Individual_7::Individual_7(double w_max) {
  w_max_ = w_max;

  metadata_ = new Metadata(this);
}

Individual_7::Individual_7(Individual_7* clone, bool no_metadata) {
  w_max_ = clone->w_max_;

  dna_ = new Dna_7(clone->dna_->length());
  //printf("DNA Factory -- %p %p\n",dna_,dna_->data_);
  dna_->set_indiv(clone->dna_,this);

  if (no_metadata) {
    metadata_ = new Metadata(this);
  } else {
    metadata_ = new Metadata(this,dynamic_cast<Metadata*>(clone->metadata_));
  }
  fitness = clone->fitness;
  metaerror = clone->metaerror;

}

Individual_7::Individual_7(double w_max,
                           char* dna_clone,
                           int32_t dna_length,
                           int32_t* lead_prom_pos,
                           int8_t* lead_prom_error,
                           int32_t lead_prom_size,
                           int32_t* lag_prom_pos,
                           int8_t* lag_prom_error,
                           int32_t lag_prom_size) {
  w_max_ = w_max;

  dna_ = new Dna_7(dna_length);
  //printf("DNA Factory -- %p %p\n",dna_,dna_->data_);
  dna_->set_indiv(dna_clone,dna_length,this);

  if (lead_prom_size == -1 && lag_prom_size == -1)
    metadata_ = new Metadata(this);
  else
    metadata_ = new Metadata(this,lead_prom_pos,lead_prom_error,lead_prom_size,lag_prom_pos,lag_prom_error,lag_prom_size);

}

Individual_7::~Individual_7() {
  delete dna_;

  delete phenotype;

  delete metadata_;
}

void Individual_7::reset_metadata() {
  delete metadata_;
  metadata_ = new Metadata(this);
}

#ifdef BASE_2
void Individual_7::search_start_protein(Rna_7* rna, int32_t pos_1, int32_t pos_2) {
        int32_t dna_length =  dna_->length_;
      char* dna_data = dna_->data_;
      #if defined(__INTEL_COMPILER)
        __declspec(align(dna_data));
      #elif defined(__INTEL_LLVM_COMPILER)
         void* vec_r =  __builtin_assume_aligned(dna_data,64);
      #endif

      int32_t rna_length = rna->length;
      bool rna_leading_lagging = rna->leading_lagging;

  if (rna->is_init_) {
        int32_t s_pos = pos_1;
        if (rna_length >= 21) {


          for (int32_t loop_size = 0; loop_size < pos_2-pos_1; loop_size++) {
            #if defined(__INTEL_COMPILER) || defined(__INTEL_LLVM_COMPILER)
            __declspec(align(64)) bool start[14] = {false,false,false,false,false,false,false,false,false,false,false,false,false,false};
            #else
            bool start[14] = {false,false,false,false,false,false,false,false,false,false,false,false,false,false};
            #endif

            int32_t c_pos = s_pos;
            if (rna_leading_lagging == 0) {
              c_pos+=loop_size;
              c_pos =
                  c_pos >= dna_length ? c_pos - dna_length
                                      : c_pos;
            } else {
              c_pos-=loop_size;
              c_pos = c_pos < 0 ? dna_length + c_pos : c_pos;
            }


            
            if (rna_leading_lagging == 0) {
              // Search for Shine Dalgarro + START codon on LEADING
              if (c_pos + 15 < dna_length && c_pos + 15 >= 0) {
                #pragma omp simd
                for (int32_t k = 0; k < 12; k++) {
                  // int32_t k_t = k >= 6 ? k + 4 : k;
                 if (dna_data[c_pos+k] == SHINE_DAL_SEQ_LEAD_7[k])
                  start[k] = true;
                }
                if (dna_data[c_pos+12] == SHINE_DAL_SEQ_LEAD_7[12]) start[12] = true;
              } else {
                for (int32_t k = 0; k < 9; k++) {
                  int32_t k_t = k >= 6 ? k + 4 : k;
                  int32_t pos_m = c_pos + k_t;

                  while (pos_m < 0)  pos_m += dna_length;
                  while (pos_m >= dna_length) pos_m -= dna_length;

                  start[k_t] = (dna_data[pos_m] == SHINE_DAL_SEQ_LEAD_7[k_t]) ? true: false;
                }
              }
            } else {
              // Search for Shine Dalgarro + START codon on LAGGING
              if (c_pos - 15 < dna_length && c_pos - 15 >= 0) {
                #pragma omp simd
                for (int32_t k = 0; k < 12; k++) {
                  // int32_t k_t = k >= 6 ? k + 4 : k;
                  if (dna_data[c_pos - k] == SHINE_DAL_SEQ_LAG_7[k])
                    start[k] = true;
                }
                if (dna_data[c_pos - 12] == SHINE_DAL_SEQ_LAG_7[12])
                    start[12] = true;
              } else {
                for (int32_t k = 0; k < 9; k++) {
                  int32_t k_t = k >= 6 ? k + 4 : k;
                  int32_t pos_m = c_pos - k_t;

                  while (pos_m < 0)  pos_m += dna_length;
                  while (pos_m >= dna_length) pos_m -= dna_length;

                  start[k_t] = (dna_data[pos_m] == SHINE_DAL_SEQ_LAG_7[k_t]) ? true : false;
                }
              }
            }

            if (start[0] && start[1] && start[2] && start[3] && start[4] && start[5] && start[10] && start[11] && start[12]) {
              rna->start_prot_count_++;
              rna->start_prot.push_back(c_pos);
            }
          }
        }
      }
      }
#endif

void Individual_7::compute_non_coding() {
  if (non_coding_computed_) return;
  non_coding_computed_ = true;

  // printf("Compute non coding\n");

  // Create a table of <genome_length> bools initialized to false (non-coding)
  int32_t genome_length = dna_->length_;

  // Including Shine-Dalgarno, spacer, START and STOP
  bool* belongs_to_CDS;
  bool* belongs_to_functional_CDS;
  bool* belongs_to_non_functional_CDS; // non-functional CDSs are those that have a null area or that lack a kind of codons (M, W or H)

  // Including Promoters and terminators
  bool* belongs_to_RNA;
  bool* belongs_to_coding_RNA;
  bool* belongs_to_non_coding_RNA;

  // Genes + prom + term (but not UTRs)
  bool* is_essential_DNA;
  bool* is_essential_DNA_including_nf_genes; // Adds non-functional genes + promoters & terminators

  bool* is_not_neutral;            // prom + term + everything in between (as opposed to neutral)


  belongs_to_CDS = new bool[genome_length];
  belongs_to_functional_CDS = new bool[genome_length];
  belongs_to_non_functional_CDS = new bool[genome_length];
  belongs_to_RNA = new bool[genome_length];
  belongs_to_coding_RNA = new bool[genome_length];
  belongs_to_non_coding_RNA = new bool[genome_length];
  is_essential_DNA = new bool[genome_length];
  is_essential_DNA_including_nf_genes = new bool[genome_length];
  is_not_neutral = new bool[genome_length];

  memset(belongs_to_CDS, 0, genome_length);
  memset(belongs_to_functional_CDS, 0, genome_length);
  memset(belongs_to_non_functional_CDS, 0, genome_length);
  memset(belongs_to_RNA, 0, genome_length);
  memset(belongs_to_coding_RNA, 0, genome_length);
  memset(belongs_to_non_coding_RNA, 0, genome_length);
  memset(is_essential_DNA, 0, genome_length);
  memset(is_essential_DNA_including_nf_genes, 0, genome_length);
  memset(is_not_neutral, 0, genome_length);


  // Parse protein lists and mark the corresponding bases as coding
  metadata_->protein_begin();
  for (int protein_idx = 0; protein_idx < metadata_->proteins_count(); protein_idx++) {
    Protein_7* prot = metadata_->protein_next();
    #ifdef __REGUL
    if (!prot->signal_ && prot->is_init_) {
    #else
    if (prot->is_init_) {
    #endif
      int32_t first;
      int32_t last;

      

      switch (prot->leading_lagging) {
        case 0:
          first = prot->protein_start;
          last = prot->protein_end;
          break;
        case 1:
          last = prot->protein_start;
          first = prot->protein_end;
          break;
        default:
          assert(false); // error: should never happen
      }

      if (first <= last) {
        for (int32_t i = first; i <= last; i++) {
          belongs_to_CDS[i] = true;
          if (prot->is_functional) is_essential_DNA[i] = true;
          is_essential_DNA_including_nf_genes[i] = true;
        }
      }
      else {
        for (int32_t i = first; i < genome_length; i++) {
          belongs_to_CDS[i] = true;
          if (prot->is_functional) is_essential_DNA[i] = true;
          is_essential_DNA_including_nf_genes[i] = true;
        }
        for (int32_t i = 0; i <= last; i++) {
          belongs_to_CDS[i] = true;
          if (prot->is_functional) is_essential_DNA[i] = true;
          is_essential_DNA_including_nf_genes[i] = true;
        }
      }

      // Include the promoter and terminator to essential DNA
      // Mark everything between promoter and terminator as not neutral
      for (Rna_7* rna : prot->rna_list_) {

        int32_t prom_first;
        int32_t prom_last;
        int32_t term_first;
        int32_t term_last;
        int32_t rna_first;
        int32_t rna_last;

        if (prot->leading_lagging == 0) { // LEADING
          prom_first = rna->begin;
          prom_last = Utils::mod(prom_first + PROM_SIZE - 1, genome_length);
          term_last = rna->end;
          term_first = Utils::mod(term_last - TERM_SIZE + 1, genome_length);
          rna_first = prom_first;
          rna_last = term_last;
        }
        else {
          prom_last = rna->begin;
          prom_first = Utils::mod(prom_last - PROM_SIZE + 1, genome_length);
          term_first = rna->end;
          term_last = Utils::mod(term_first + TERM_SIZE - 1, genome_length);
          rna_first = term_first;
          rna_last = prom_last;
        }

        // Let us begin with "non-neutral" regions...
        if (rna_first <= rna_last) {
          for (int32_t i = rna_first;
               i <= rna_last; i++) { is_not_neutral[i] = true; }
        }
        else {
          for (int32_t i = rna_first;
               i < genome_length; i++) { is_not_neutral[i] = true; }
          for (int32_t i = 0; i <= rna_last; i++) { is_not_neutral[i] = true; }
        }

        // ...and go on with essential DNA
        if (prom_first <= prom_last) {
          for (int32_t i = prom_first; i <= prom_last; i++) {
            //~ printf("%ld ", i);
            if (prot->is_functional) is_essential_DNA[i] = true;
            is_essential_DNA_including_nf_genes[i] = true;
          }
        }
        else {
          for (int32_t i = prom_first; i < genome_length; i++) {
            //~ printf("%ld ", i);
            if (prot->is_functional) is_essential_DNA[i] = true;
            is_essential_DNA_including_nf_genes[i] = true;
          }
          for (int32_t i = 0; i <= prom_last; i++) {
            //~ printf("%ld ", i);
            if (prot->is_functional) is_essential_DNA[i] = true;
            is_essential_DNA_including_nf_genes[i] = true;
          }
        }
        //~ printf("\n");

        //~ printf("term ");
        if (term_first <= term_last) {
          for (int32_t i = term_first; i <= term_last; i++) {
            //~ printf("%ld ", i);
            if (prot->is_functional) is_essential_DNA[i] = true;
            is_essential_DNA_including_nf_genes[i] = true;
          }
        }
        else {
          for (int32_t i = term_first; i < genome_length; i++) {
            //~ printf("%ld ", i);
            if (prot->is_functional) is_essential_DNA[i] = true;
            is_essential_DNA_including_nf_genes[i] = true;
          }
          for (int32_t i = 0; i <= term_last; i++) {
            //~ printf("%ld ", i);
            if (prot->is_functional) is_essential_DNA[i] = true;
            is_essential_DNA_including_nf_genes[i] = true;
          }
        }
        //~ printf("\n");
        //~ getchar();
      }


      if (prot->is_functional) {
        if (first <= last) {
          for (int32_t i = first; i <= last; i++) {
            belongs_to_functional_CDS[i] = true;
          }
        }
        else {
          for (int32_t i = first; i < genome_length; i++) {
            belongs_to_functional_CDS[i] = true;
          }
          for (int32_t i = 0; i <= last; i++) {
            belongs_to_functional_CDS[i] = true;
          }
        }
      }
      else // degenerated protein
      {
        if (first <= last) {
          for (int32_t i = first; i <= last; i++) {
            belongs_to_non_functional_CDS[i] = true;
          }
        }
        else {
          for (int32_t i = first; i < genome_length; i++) {
            belongs_to_non_functional_CDS[i] = true;
          }
          for (int32_t i = 0; i <= last; i++) {
            belongs_to_non_functional_CDS[i] = true;
          }
        }
      }
    }
  }


  // Parse RNA lists and mark the corresponding bases as coding (only for the coding RNAs)
  // TODO vld: this block cries for refactoring
  metadata_->rna_begin();

  for (int rna_idx = 0; rna_idx <
                        (int)metadata_->rna_count(); rna_idx++) {
      Rna_7* rna =
          metadata_->rna_next();
      int32_t first;
      int32_t last;

      if (rna->leading_lagging == 0) {
        first = rna->begin;
        last = rna->end;
      }
      else { // (strand == LAGGING)
        first = rna->end;
        last = rna->begin;
      }

      assert(first < genome_length);
      assert(last < genome_length);

      if (first <= last) {
        for (int32_t i = first; i <= last; i++) {
          belongs_to_RNA[i] = true;
        }
      }
      else {
        for (int32_t i = first; i < genome_length; i++)
          belongs_to_RNA[i] = true;
        for (int32_t i = 0; i <= last; i++)
          belongs_to_RNA[i] = true;
      }

      bool is_coding_rna = false;
      metadata_->protein_begin();
      for (int protein_idx = 0; protein_idx < metadata_->proteins_count(); protein_idx++) {
        Protein_7* prot = metadata_->protein_next();
        if (prot->is_init_) {
          for (auto i_rna : prot->rna_list_) {
            if (i_rna->begin == rna->begin && i_rna->end == rna->end && i_rna->leading_lagging == rna->leading_lagging) {
              is_coding_rna = true;
            }

          }
        }
      }


      if (is_coding_rna) { // coding RNA
        if (first <= last) {
          for (int32_t i = first; i <= last; i++)
            belongs_to_coding_RNA[i] = true;
        }
        else {
          for (int32_t i = first; i < genome_length; i++)
            belongs_to_coding_RNA[i] = true;
          for (int32_t i = 0; i <= last; i++)
            belongs_to_coding_RNA[i] = true;
        }
      }
      else // non coding RNA
      {
        if (first <= last) {
          for (int32_t i = first; i <= last; i++)
            belongs_to_non_coding_RNA[i] = true;
        }
        else {
          for (int32_t i = first; i < genome_length; i++)
            belongs_to_non_coding_RNA[i] = true;
          for (int32_t i = 0; i <= last; i++)
            belongs_to_non_coding_RNA[i] = true;
        }
      }
    }
  

  // Count non-coding bases
  nb_bases_in_0_CDS_ = 0;
  nb_bases_in_0_functional_CDS_ = 0;
  nb_bases_in_0_non_functional_CDS_ = 0;
  nb_bases_in_0_RNA_ = 0;
  nb_bases_in_0_coding_RNA_ = 0;
  nb_bases_in_0_non_coding_RNA_ = 0;
  nb_bases_non_essential_ = 0;
  nb_bases_non_essential_including_nf_genes_ = 0;
  nb_bases_in_neutral_regions_ = 0;
  nb_neutral_regions_ = 0;

  // We do not know how many neutral regions there will be, but
  // there should be less than nb_coding_RNAs_ + 1
  // As we will see, there may be a shift in values so we take size nb_coding_RNAs_ + 2
  int32_t* tmp_beginning_neutral_regions = new int32_t[nb_coding_RNAs + 2];
  int32_t* tmp_end_neutral_regions = new int32_t[nb_coding_RNAs + 2];
  memset(tmp_beginning_neutral_regions, -1, nb_coding_RNAs + 2);
  memset(tmp_end_neutral_regions, -1, nb_coding_RNAs + 2);

  for (int32_t i = 0; i < genome_length; i++) {
    if (belongs_to_CDS[i] == false) {
      nb_bases_in_0_CDS_++;
    }
    if (belongs_to_functional_CDS[i] == false) {
      nb_bases_in_0_functional_CDS_++;
    }
    if (belongs_to_non_functional_CDS[i] == false) {
      nb_bases_in_0_non_functional_CDS_++;
    }
    if (belongs_to_RNA[i] == false) {
      nb_bases_in_0_RNA_++;
    }
    if (belongs_to_coding_RNA[i] == false) {
      nb_bases_in_0_coding_RNA_++;
    }
    if (belongs_to_non_coding_RNA[i] == false) {
      nb_bases_in_0_non_coding_RNA_++;
    }
    if (is_essential_DNA[i] == false) {
      nb_bases_non_essential_++;
    }
    if (is_essential_DNA_including_nf_genes[i] == false) {
      nb_bases_non_essential_including_nf_genes_++;
    }
    if (is_not_neutral[i] == false) {
      nb_bases_in_neutral_regions_++;
    }
    if (i != 0) {
      if (is_not_neutral[i] != is_not_neutral[i - 1]) {
        if (is_not_neutral[i - 1] == true) // beginning of a neutral region
        {
          tmp_beginning_neutral_regions[nb_neutral_regions_] = i;
        }
        else // end of a neutral region
        {
          tmp_end_neutral_regions[nb_neutral_regions_] = i - 1;
          nb_neutral_regions_++;
        }
      }
    }
    else // i = 0
    {
      // we arbitrarily set 0 as the beginning of a neutral region (linkage with end of genetic unit
      // will be done later)
      if (is_not_neutral[0] ==
          false) { tmp_beginning_neutral_regions[nb_neutral_regions_] = 0; }
    }
  }

  // we have to treat specifically the last base of the genetic unit in order to link neutral regions
  // at the end and the beginning of genetic unit
  int32_t shift = 0;
  if (is_not_neutral[genome_length - 1] == false) {
    if (is_not_neutral[0] == true) // end of a neutral region
    {
      tmp_end_neutral_regions[nb_neutral_regions_] = genome_length - 1;
      nb_neutral_regions_++;
    }
    else // neutral region goes on after base 0, linkage to be done
    {
      if (nb_neutral_regions_ != 0) {
        tmp_end_neutral_regions[nb_neutral_regions_] = tmp_end_neutral_regions[0];
        // the first neutral region is only a subpart of the last one, it should not be
        // taken into account. When we transfer values to the final array, we introduce a shift
        shift = 1;
        // we do not ++ nb_neutral_regions_ as it was already counted
      }
      else // no neutral region detected until now -> all the genetic unit is neutral
      {
        // as all the chromosome is neutral, we indicate 0 as the beginning of the region
        // and genome_length - 1 as its end
        tmp_end_neutral_regions[0] = genome_length - 1;
        nb_neutral_regions_++;
      }
    }
  }

  // now that we know how many neutral regions there are, we can transfer data to correctly sized arrays
  assert(nb_neutral_regions_ <= nb_coding_RNAs + 1);
  if (beginning_neutral_regions_ !=
      NULL) { delete[] beginning_neutral_regions_; }
  if (end_neutral_regions_ != NULL) { delete[] end_neutral_regions_; }

  if (nb_neutral_regions_ >
      0) // as unlikely as it seems, there may be no neutral region
  {
    beginning_neutral_regions_ = new int32_t[nb_neutral_regions_];
    end_neutral_regions_ = new int32_t[nb_neutral_regions_];
    // transfer from tmp to attributes
    for (int32_t i = 0; i < nb_neutral_regions_; i++) {
      beginning_neutral_regions_[i] = tmp_beginning_neutral_regions[i + shift];
      end_neutral_regions_[i] = tmp_end_neutral_regions[i + shift];
    }
  }
  else // nb_neutral_regions_ == 0
  {
    beginning_neutral_regions_ = NULL;
    end_neutral_regions_ = NULL;
  }

  // printf("nb_bases_in_0_CDS_ %d\n",nb_bases_in_0_CDS_);

  delete[] tmp_beginning_neutral_regions;
  delete[] tmp_end_neutral_regions;

  delete[] belongs_to_CDS;
  delete[] belongs_to_functional_CDS;
  delete[] belongs_to_non_functional_CDS;
  delete[] belongs_to_RNA;
  delete[] belongs_to_coding_RNA;
  delete[] belongs_to_non_coding_RNA;
  delete[] is_essential_DNA;
  delete[] is_essential_DNA_including_nf_genes;
  delete[] is_not_neutral;
}

void Individual_7::save_to_backup(gzFile& backup_file) const {
  length_t length = dna_->length_;
  gzwrite(backup_file, &(length), sizeof(length_t));
  gzwrite(backup_file, dna_->data_, length * sizeof(base_t));
}

void Individual_7::load_from_backup(gzFile& backup_file) {
  // assuming the individual is created without any dna
  length_t length;
  gzread(backup_file, &length, sizeof(length));

  auto* seq = new base_t[length];
  gzread(backup_file, seq, length * sizeof(base_t));

  dna_ = new Dna_7(length);
  dna_->set_indiv(seq, length, nullptr);
  delete[] seq;
}

void Individual_7::start_stop_RNA() {
  for (int dna_pos = 0; dna_pos < dna_->length(); dna_pos++) {
    int len = dna_->length();
    if (len >= PROM_SIZE) {
#ifdef BASE_2
      int prom_dist_leading[26];
      int prom_dist_lagging[26];

      int term_dist_leading[4];
      int term_dist_lagging[4];

      for (int motif_id = 0; motif_id < 52; motif_id++) {
        if (motif_id >= 26 && motif_id < 48) {
          // LAGGING
          int t_motif_id = motif_id - 26;
          prom_dist_lagging[t_motif_id] =
              PROM_SEQ_LAG[t_motif_id] ==
                      dna_->data_[
                          dna_pos - t_motif_id < 0 ? len +
                                                         dna_pos -
                                                         t_motif_id :
                                                   dna_pos - t_motif_id]
                  ? 0 : 1;
        } else if (motif_id < 22) {
          // LEADING
          prom_dist_leading[motif_id] =
              PROM_SEQ_LEAD[motif_id] ==
                      dna_->data_[
                          dna_pos + motif_id >= len ? dna_pos +
                                                          motif_id -
                                                          len
                                                    : dna_pos +
                                                          motif_id]
                  ? 0
                  : 1;
        } else if (motif_id >= 22 && motif_id < 26) {
          int t_motif_id = motif_id - 22;
          // LEADING
          term_dist_leading[t_motif_id] =
              dna_->data_[
                  dna_pos + t_motif_id >= len ? dna_pos +
                                                    t_motif_id -
                                                    len :
                                              dna_pos + t_motif_id] !=
                      dna_->data_[
                          dna_pos - t_motif_id + 10 >= len ?
                                                           dna_pos - t_motif_id + 10 - len :
                                                           dna_pos -
                                                               t_motif_id +
                                                               10] ? 1
                  : 0;
        } else {
          int t_motif_id = motif_id - 48;
          term_dist_lagging[t_motif_id] =
              dna_->data_[
                  dna_pos - t_motif_id < 0 ? dna_pos -
                                                 t_motif_id +
                                                 len
                                           : dna_pos -
                                                 t_motif_id] !=
                      dna_->data_[
                          dna_pos + t_motif_id - 10 < 0 ? dna_pos +
                                                              t_motif_id -
                                                              10 + len
                                                        :
                                                        dna_pos + t_motif_id - 10] ? 1 : 0;
        }
      }


      int i_prom_dist_leading = prom_dist_leading[0] +
                                prom_dist_leading[1] +
                                prom_dist_leading[2] +
                                prom_dist_leading[3] +
                                prom_dist_leading[4] +
                                prom_dist_leading[5] +
                                prom_dist_leading[6] +
                                prom_dist_leading[7] +
                                prom_dist_leading[8] +
                                prom_dist_leading[9] +
                                prom_dist_leading[10] +
                                prom_dist_leading[11] +
                                prom_dist_leading[12] +
                                prom_dist_leading[13] +
                                prom_dist_leading[14] +
                                prom_dist_leading[15] +
                                prom_dist_leading[16] +
                                prom_dist_leading[17] +
                                prom_dist_leading[18] +
                                prom_dist_leading[19] +
                                prom_dist_leading[20] +
                                prom_dist_leading[21];

      int i_prom_dist_lagging = prom_dist_lagging[0] +
                                prom_dist_lagging[1] +
                                prom_dist_lagging[2] +
                                prom_dist_lagging[3] +
                                prom_dist_lagging[4] +
                                prom_dist_lagging[5] +
                                prom_dist_lagging[6] +
                                prom_dist_lagging[7] +
                                prom_dist_lagging[8] +
                                prom_dist_lagging[9] +
                                prom_dist_lagging[10] +
                                prom_dist_lagging[11] +
                                prom_dist_lagging[12] +
                                prom_dist_lagging[13] +
                                prom_dist_lagging[14] +
                                prom_dist_lagging[15] +
                                prom_dist_lagging[16] +
                                prom_dist_lagging[17] +
                                prom_dist_lagging[18] +
                                prom_dist_lagging[19] +
                                prom_dist_lagging[20] +
                                prom_dist_lagging[21];

      int i_term_leading = term_dist_leading[0] +
                           term_dist_leading[1] +
                           term_dist_leading[2] +
                           term_dist_leading[3];


      int i_term_lagging = term_dist_lagging[0] +
                           term_dist_lagging[1] +
                           term_dist_lagging[2] +
                           term_dist_lagging[3];

#elif BASE_4
      int i_prom_dist_leading = metadata_->is_promoter_leading(dna_pos);
      int i_prom_dist_lagging = metadata_->is_promoter_lagging(dna_pos);
      int i_term_leading = metadata_->is_terminator_leading(dna_pos);
      int i_term_lagging = metadata_->is_terminator_lagging(dna_pos);
#endif

      if (i_prom_dist_leading <= PROM_MAX_DIFF) {
        PromoterStruct* nprom = new PromoterStruct(dna_pos, i_prom_dist_leading,
                                                   true);
        int prom_idx;

        prom_idx = metadata_->promoter_count();
        metadata_->set_promoters_count(
            metadata_->promoter_count()+ 1);

        metadata_->promoter_add(prom_idx, nprom);

        delete nprom;
      }


#ifdef BASE_2
      if (i_term_leading == 4) {
#elif BASE_4
      if (i_term_leading) {
#endif
        metadata_->terminator_add(LEADING, dna_pos);
      }


      if (i_prom_dist_lagging <= PROM_MAX_DIFF) {
        PromoterStruct* nprom = new PromoterStruct(dna_pos, i_prom_dist_lagging,
                                                   false);
        int prom_idx;
        prom_idx = metadata_->promoter_count();
        metadata_->set_promoters_count(
            metadata_->promoter_count() + 1);

        metadata_->promoter_add(prom_idx, nprom);
        delete nprom;
      }




#ifdef BASE_2
      if (i_term_lagging == 4) {
#elif BASE_4
      if (i_term_lagging) {
#endif
        metadata_->terminator_add(LAGGING, dna_pos);
      }
    }
  }
}

void Individual_7::opt_prom_compute_RNA() {
  metadata_->proteins_clear();
  metadata_->rnas_clear();
  metadata_->terminators_clear();

  metadata_->rnas_resize(
      metadata_->promoter_count());

  metadata_->promoter_begin();

#ifdef BASE_4
  int8_t term_size = TERM_SIZE + Dna_7::polya_length;
#endif

  // ANNOTATE_SITE_BEGIN(mainloop);
  for (int prom_idx = 0;
       prom_idx < (int)metadata_->promoter_count(); prom_idx++) {
    PromoterStruct*prom =
        metadata_->promoter_next();

    if (prom != nullptr) {
#ifdef WITH_OPTIMIZE_DIFF_SEARCH
      if (prom->to_compute_end_ ) {
#else
      if (true) {
#endif
        Dna_7*dna = dna_;
        int32_t dna_length = dna->length();
        int prom_pos;
        bool lead_lag;
        double prom_error;

        prom_pos = prom->pos;
        lead_lag = prom->leading_or_lagging;
        prom_error = fabs(
            ((float) prom->error));

        int32_t length = dna->length_;
        char *dna_data = dna->data_;
#if defined(__INTEL_COMPILER)
        __declspec(align(dna_data));
#elif defined(__INTEL_LLVM_COMPILER)
        void* vec_r = __builtin_assume_aligned(dna_data,64);
#endif

        if (lead_lag) {
          /* Search for terminators */
          int cur_pos =
              Utils::mod(prom_pos + PROM_SIZE,dna_length);
          int start_pos = cur_pos;

          bool terminator_found = false;
          bool no_terminator = false;
          int8_t term_dist_leading = 0;

          int loop_size = 0;


          while (!terminator_found) {
#ifdef BASE_2
            // ANNOTATE_SITE_BEGIN(leading_motif_loop);
            if (cur_pos + 10 < length && cur_pos + 10 >= 0) {
  #pragma omp simd
              for (int32_t t_motif_id = 0; t_motif_id < 4; t_motif_id++) {
                int32_t pos_begin = cur_pos + t_motif_id;
                int32_t pos_end   = cur_pos - t_motif_id + 10;

                // ANNOTATE_ITERATION_TASK(searchLeading);
                term_dist_leading +=
                    dna_data[pos_begin] !=
                            dna_data[pos_end]
                        ? 1 : 0;
              }
            } else {
              for (int32_t t_motif_id = 0; t_motif_id < 4; t_motif_id++) {
                int32_t pos_begin = cur_pos + t_motif_id;
                int32_t pos_end   = cur_pos - t_motif_id + 10;

                while (pos_begin < 0)       pos_begin += length;
                while (pos_begin >= length) pos_begin -= length;

                while (pos_end < 0)       pos_end += length;
                while (pos_end >= length) pos_end -= length;

                // ANNOTATE_ITERATION_TASK(searchLeading);
                term_dist_leading +=
                    dna_data[pos_begin] !=
                            dna_data[pos_end]
                        ? 1 : 0;
              }
            }

            // ANNOTATE_SITE_END(leading_motif_loop);

            if (term_dist_leading == 4) {
              terminator_found = true;
            }
            else {
              cur_pos = Utils::mod(cur_pos + 1,dna_length);

              term_dist_leading = 0;
              if (cur_pos == start_pos) {
                no_terminator = true;
                terminator_found = true;
              }
            }


#elif BASE_4
            terminator_found = metadata_->is_terminator_leading(cur_pos);

            if(!terminator_found) {
              cur_pos = cur_pos + 1 >= dna_length ?
                                                  cur_pos + 1 - dna_length : cur_pos + 1;

              if (cur_pos == start_pos) {
                no_terminator = true;
                terminator_found = true;
              }
            }
#endif

            loop_size++;

          }

          if (!no_terminator) {
#ifdef BASE_2
            int32_t rna_end =
                cur_pos;
            int32_t rna_length = 0;

            rna_length = loop_size + TERM_SIZE -1;

            if (rna_length > 0) {


              int glob_rna_idx = -1;
              glob_rna_idx =
                  metadata_->rna_count_++;
              metadata_->rna_add(glob_rna_idx,
                                        prom_pos,
                                        rna_end,
                                        !lead_lag,
                                        1.0 -
                                            prom_error /
                                                5.0, rna_length,prom);

            }
#elif BASE_4
            int32_t rna_end = Utils::mod(cur_pos + term_size, dna_length);
            int32_t rna_length = loop_size-1 + term_size;

            if (rna_length >= term_size) {  // check that terminator is not overlapping with promoter
              int glob_rna_idx = metadata_->rna_count_++;
              metadata_->rna_add(glob_rna_idx,
                                        prom_pos,
                                        rna_end,
                                        !lead_lag,
                                        BASAL_LEVELS[prom_error],
                                        rna_length, prom);
            }
#endif
          }
        } else {
          /* Search for terminator */
          int cur_pos =
              prom_pos - 22;
          cur_pos =
              cur_pos < 0 ? dna_length + (cur_pos) : cur_pos;
          int start_pos = cur_pos;
          bool terminator_found = false;
          bool no_terminator = false;
          int term_dist_lagging = 0;

          int loop_size = 0;

          while (!terminator_found) {
#ifdef BASE_2
            // ANNOTATE_SITE_BEGIN(lagging_motif_loop);
            if (cur_pos - 10 < length && cur_pos - 10 >= 0) {
  #pragma omp simd
              for (int32_t t_motif_id = 0; t_motif_id < 4; t_motif_id++) {
                int32_t pos_begin = cur_pos - t_motif_id;
                int32_t pos_end   = cur_pos + t_motif_id - 10;

                // ANNOTATE_ITERATION_TASK(searchLeading);
                term_dist_lagging +=
                    dna_data[pos_begin] !=
                            dna_data[pos_end]
                        ? 1 : 0;
              }
            } else {
              for (int32_t t_motif_id = 0; t_motif_id < 4; t_motif_id++) {
                int32_t pos_begin = cur_pos - t_motif_id;
                int32_t pos_end   = cur_pos + t_motif_id - 10;

                while (pos_begin < 0)       pos_begin += length;
                while (pos_begin >= length) pos_begin -= length;

                while (pos_end < 0)       pos_end += length;
                while (pos_end >= length) pos_end -= length;

                // ANNOTATE_ITERATION_TASK(searchLeading);
                term_dist_lagging +=
                    dna_data[pos_begin] !=
                            dna_data[pos_end]
                        ? 1 : 0;
              }
            }


            // ANNOTATE_SITE_END(lagging_motif_loop);

            if (term_dist_lagging == 4) {
              terminator_found = true;
            }
            else {
              cur_pos =
                  cur_pos - 1 < 0 ? dna_length + (cur_pos - 1)
                                  : cur_pos - 1;
              term_dist_lagging = 0;
              if (cur_pos == start_pos) {
                no_terminator = true;
                terminator_found = true;
              }
            }
#elif BASE_4
            terminator_found = metadata_->is_terminator_lagging(cur_pos);

            if(!terminator_found) {
              cur_pos = cur_pos - 1 < 0 ?
                                        dna_length + (cur_pos - 1) : cur_pos - 1;

              if (cur_pos == start_pos) {
                no_terminator = true;
                terminator_found = true;
              }
            }
#endif
            loop_size++;
          }

          if (!no_terminator) {
#ifdef BASE_2

            int32_t rna_end = Utils::mod(cur_pos - 10,dna_length);

            int32_t rna_length = 0;
            rna_length = loop_size + TERM_SIZE -1;

            if (rna_length >= 0) {
              int glob_rna_idx = -1;
              glob_rna_idx =
                  metadata_->rna_count_++;

              metadata_->rna_add(glob_rna_idx,
                                        prom_pos,
                                        rna_end,
                                        !lead_lag,
                                        1.0 -
                                            prom_error /
                                                5.0, rna_length,prom);
            }

#elif BASE_4
            int32_t rna_end = Utils::mod(cur_pos - term_size, dna_length);
            int32_t rna_length = loop_size-1 + term_size;

            if (rna_length >= term_size) {
              int glob_rna_idx = -1;
              glob_rna_idx =
                  metadata_->rna_count_++;

              metadata_->rna_add(glob_rna_idx,
                                        prom_pos,
                                        rna_end,
                                        !lead_lag,
                                        BASAL_LEVELS[prom_error],
                                        rna_length,prom);
            }
#endif
          }
        }
      }
    }
  }
  // ANNOTATE_SITE_END(mainloop);
}

void Individual_7::compute_RNA() {
  metadata_->rnas_resize(metadata_->promoter_count());
  int32_t dna_length = dna_->length();
  
#ifdef BASE_2
  int8_t term_size = 10;
#elif BASE_4
  int8_t term_size = TERM_SIZE + Dna_7::polya_length;
#endif

#ifdef WITH_FINETASKLOOP
#pragma omp taskloop grainsize(rna_grain_size)
#endif
  for (int rna_idx = 0; rna_idx <
                        (int)metadata_->promoter_count(); rna_idx++) {
    {
      if (metadata_->promoters(rna_idx) != nullptr) {
        if (metadata_->promoters(rna_idx)->leading_or_lagging) {
          if (metadata_->terminator_count(LEADING) != 0) {


            int k = metadata_->promoters(rna_idx)->pos + PROM_SIZE;

            k = k >= dna_length ? k - dna_length : k;

            int32_t next_rna_end =
                metadata_->next_terminator(LEADING,k);

            int32_t rna_end =
                next_rna_end + term_size >= dna_length ?
                                                       next_rna_end + term_size - dna_length :
                                                       next_rna_end + term_size;

            int32_t rna_length = 0;

#ifdef BASE_2
            if (k > next_rna_end)
              rna_length = dna_length + next_rna_end - k;
            else
              rna_length = next_rna_end - k;

            rna_length += TERM_SIZE;

            if (rna_length >= 0) {
#elif BASE_4
            if (metadata_->promoters(rna_idx)->pos > rna_end) {
              rna_length = dna_length -
                           metadata_->promoters(rna_idx)->pos +
                           rna_end + 1;
            } else {
              rna_length =
                  rna_end - metadata_->promoters(rna_idx)->pos + 1;
            }
            rna_length -= PROM_SIZE;

            if (rna_length >= term_size) { // check that terminator is not overlapping with promoter
#endif

              int glob_rna_idx = -1;
              glob_rna_idx =
                  metadata_->rna_count();
              metadata_->set_rna_count(
                  metadata_->rna_count() + 1);
              

              metadata_->rna_add(glob_rna_idx, new Rna_7(metadata_->promoters(rna_idx)->pos,
                                                                rna_end,
                                                                !metadata_->promoters(rna_idx)->leading_or_lagging,
#ifdef BASE_2
                                                                1.0 -
                                                                    fabs(
                                                                        ((float)metadata_->promoters(rna_idx)->error)) /
                                                                        5.0, rna_length,metadata_->promoters(rna_idx)));
#elif BASE_4
                                                                BASAL_LEVELS[abs(metadata_->promoters(rna_idx)->error)],
                                                                rna_length,metadata_->promoters(rna_idx)));
#endif
            }
          }
        } else {
          // LAGGING
          if (metadata_->terminator_count(LAGGING) != 0) {
            // Search for terminator
            int k = metadata_->promoters(rna_idx)->pos - PROM_SIZE;
            k = k < 0 ? dna_length + k : k;

            int32_t next_rna_end =
                metadata_->next_terminator(LAGGING,k);


            int32_t rna_end =
                next_rna_end - term_size < 0 ? dna_length + (next_rna_end - term_size)
                                             :
                                             next_rna_end - term_size;

            int32_t rna_length = 0;
            
#ifdef BASE_2
            if (k < next_rna_end)
              rna_length = k + dna_length - next_rna_end;
            else
              rna_length = k - next_rna_end;

            rna_length += TERM_SIZE;


            if (rna_length >= 0) {
#elif BASE_4
            if (metadata_->promoters(rna_idx)->pos < rna_end) {
              rna_length = metadata_->promoters(rna_idx)->pos +
                           dna_length - rna_end + 1;
            }
            else {
              rna_length =
                  metadata_->promoters(rna_idx)->pos - rna_end + 1;
            }

            rna_length -= PROM_SIZE;

            if (rna_length >= term_size) { // check that terminator is not overlapping with promoter
#endif

              int glob_rna_idx;
              glob_rna_idx =
                  metadata_->rna_count();
              metadata_->set_rna_count(
                  metadata_->rna_count() + 1);
              

              metadata_->rna_add(glob_rna_idx, new Rna_7(metadata_->promoters(rna_idx)->pos,
                                                                rna_end,
                                                                !metadata_->promoters(rna_idx)->leading_or_lagging,
#ifdef BASE_2
                                                                1.0 -
                                                                    fabs(
                                                                        ((float)metadata_->promoters(rna_idx)->error)) /
                                                                        5.0, rna_length,metadata_->promoters(rna_idx)));
#elif BASE_4
                                                                BASAL_LEVELS[abs(metadata_->promoters(rna_idx)->error)],
                                                                rna_length,metadata_->promoters(rna_idx)));
#endif

            }
          }
        }
      }
    }
  }
}

void Individual_7::start_protein() {
  metadata_->rna_begin();
#ifdef BASE_4
  int8_t term_size = TERM_SIZE + Dna_7::polya_length;
#endif
  
  for (int rna_idx = 0; rna_idx <
                        (int)metadata_->rna_count(); rna_idx++) {
    Rna_7* rna =
        metadata_->rna_next();

    int32_t dna_length = dna_->length_;
      
#ifdef BASE_2
  #ifdef WITH_OPTIMIZE_DIFF_SEARCH
    if (true) {
  #else
    if (rna->to_compute_start_pos_) {
  #endif
          
      rna->start_prot.clear();
      rna->start_prot_count_ = 0;
          
      char* dna_data = dna_->data_;
  #if defined(__INTEL_COMPILER)
      __declspec(align(dna_data));
  #elif defined(__INTEL_LLVM_COMPILER)
      void* vec_r = __builtin_assume_aligned(dna_data,64);
  #endif

      int32_t rna_length = rna->length;
      bool rna_leading_lagging = rna->leading_lagging;

      if (rna->is_init_) {
        int32_t s_pos = rna->begin;
        if (rna_length >= 21) {
          if (rna_leading_lagging == 0) {
            s_pos += 22;
            s_pos =
                s_pos >= dna_length ? s_pos - dna_length
                                    : s_pos;
          } else {
            s_pos -= 22;
            s_pos =
                s_pos < 0 ? ((int) dna_length) + s_pos : s_pos;
          }


          int32_t loop_size = 0;

  #ifdef __VECTORIZE_STRCMP
          char buffer_str[13];
  #endif
    
          for (int32_t loop_size = 0; loop_size < rna_length - DO_TRANSLATION_LOOP; loop_size++) {

  #ifdef __VECTORIZE_STRCMP
            bool b_start = false;
            bool start[14] = {false,false,false,false,false,false,false,false,false,false,false,false,false,false};
  #else
    #if defined(__INTEL_COMPILER) || defined(__INTEL_LLVM_COMPILER)
            __declspec(align(64)) bool start[14] = {false,false,false,false,false,false,false,false,false,false,false,false,false,false};
    #else
            bool start[14] = {false,false,false,false,false,false,false,false,false,false,false,false,false,false};
    #endif
  #endif

            int32_t c_pos = s_pos;
            if (rna_leading_lagging == 0) {
              c_pos+=loop_size;
              c_pos =
                  c_pos >= dna_length ? c_pos - dna_length
                                      : c_pos;
            } else {
              c_pos-=loop_size;
              c_pos = c_pos < 0 ? dna_length + c_pos : c_pos;
            }

            if (rna_leading_lagging == 0) {
              // Search for Shine Dalgarro + START codon on LEADING
              if (c_pos + 15 < dna_length && c_pos + 15 >= 0) {
  #ifdef __VECTORIZE_STRCMP
                bool compare_1 = strncmp(&(dna_data[c_pos]),SHINE_DAL_SEQ_LEAD_7,6) == 0 ? true : false;
                bool compare_2 = strncmp(&(dna_data[c_pos+10]),&(SHINE_DAL_SEQ_LEAD_7[10]),3) == 0 ? true : false;
                b_start = compare_1 && compare_2;
  #else
    // char c_sub[13] = {};
    #if defined(__INTEL_COMPILER) || defined(__INTEL_LLVM_COMPILER)
      #pragma omp simd aligned(dna_data:64) aligned(SHINE_DAL_SEQ_LEAD_7:64) aligned(start:64)
    #else
      #pragma omp simd
    #endif
                for (int32_t k = 0; k < 12; k++) {
                  start[k] = dna_data[c_pos+k] == SHINE_DAL_SEQ_LEAD_7[k];
                }
                start[12] = dna_data[c_pos+12] == SHINE_DAL_SEQ_LEAD_7[12];
  #endif
              } else {
                
                for (int32_t k = 0; k < 9; k++) {
                  int32_t k_t = k >= 6 ? k + 4 : k;
                  int32_t pos_m = c_pos + k_t;

                  while (pos_m < 0)  pos_m += dna_length;
                  while (pos_m >= dna_length) pos_m -= dna_length;

                  start[k_t] = (dna_data[pos_m] == SHINE_DAL_SEQ_LEAD_7[k_t]) ? true: false;
                }
  #ifdef __VECTORIZE_STRCMP
                b_start = start[0] && start[1] && start[2] && start[3] && start[4] && start[5] && start[10] && start[11] && start[12];
  #endif
              }

            } else {
              // Search for Shine Dalgarro + START codon on LAGGING
              if (c_pos - 15 < dna_length && c_pos - 15 >= 0) {
  #ifdef __VECTORIZE_STRCMP
                for (int i = 0; i < 13; i++) buffer_str[i] = dna_data[c_pos-i];
                bool compare_1 = strncmp(buffer_str,SHINE_DAL_SEQ_LAG_7,6) == 0 ? true : false;
                bool compare_2 = strncmp(&(buffer_str[10]),&(SHINE_DAL_SEQ_LAG_7[10]),3) == 0 ? true : false;

                b_start = compare_1 && compare_2;
  #else
    #pragma omp simd
                for (int32_t k = 0; k < 12; k++) {
                  start[k] = dna_data[c_pos - k] == SHINE_DAL_SEQ_LAG_7[k];
                }
                start[12] = dna_data[c_pos - 12] == SHINE_DAL_SEQ_LAG_7[12];
  #endif    
              } else {
                for (int32_t k = 0; k < 9; k++) {
                  int32_t k_t = k >= 6 ? k + 4 : k;
                  int32_t pos_m = c_pos - k_t;

                  while (pos_m < 0)  pos_m += dna_length;
                  while (pos_m >= dna_length) pos_m -= dna_length;

                  start[k_t] = (dna_data[pos_m] == SHINE_DAL_SEQ_LAG_7[k_t]) ? true : false;

                }
  #ifdef __VECTORIZE_STRCMP
                b_start = start[0] && start[1] && start[2] && start[3] && start[4] && start[5] && start[10] && start[11] && start[12];
  #endif
              }
            }

  #ifdef __VECTORIZE_STRCMP
            if (b_start) {
  #else
            if (start[0] && start[1] && start[2] && start[3] && start[4] && start[5] && start[10] && start[11] && start[12]) {
  #endif
              rna->start_prot_count_++;
              rna->start_prot.push_back(c_pos);

            }
          }
        }
      }
    }
  }
#elif BASE_4

    if (rna->is_init_ && rna->length >= DO_TRANSLATION_LOOP) {
      int c_pos = rna->begin;

      if(rna->leading_lagging == LEADING) {
        c_pos += PROM_SIZE;
        c_pos =
            c_pos >= dna_length ? c_pos - dna_length : c_pos;
      } else {
        c_pos -= PROM_SIZE;
        c_pos =
            c_pos < 0 ? dna_length + c_pos : c_pos;
      }

      int loop_size = 0;
      while (loop_size+DO_TRANSLATION_LOOP < rna->length) {
        bool start = false;

        if (rna->leading_lagging == LEADING) {
          start = metadata_->is_shine_dal_start_prot_leading(c_pos);
        } else {
          start = metadata_->is_shine_dal_start_prot_lagging(c_pos);
        }

        if (start) {
          rna->start_prot_count_++;
          rna->start_prot.push_back(c_pos);
        }

        if (rna->leading_lagging == LEADING) {
          c_pos++;
          c_pos =
              c_pos >= dna_length ? c_pos - dna_length
                                  : c_pos;
        } else {
          c_pos--;
          c_pos = c_pos < 0 ? dna_length + c_pos : c_pos;
        }
        loop_size++;
      }
    }
  }
#endif
}

void Individual_7::compute_protein() {
  int resize_to = 0;
  
    
  metadata_->rna_begin();
    
  // ANNOTATE_SITE_BEGIN(mainloop);
  for (int prom_idx = 0;
       prom_idx < (int)metadata_->rna_count(); prom_idx++) {
    //  ANNOTATE_ITERATION_TASK(searchMain);
    Rna_7* rna =
        metadata_->rna_next();

    if (rna->is_init_)
      resize_to += rna->start_prot_count_;
  }

  
      metadata_->proteins_resize(resize_to);

  Dna_7* dna = dna_;
  int32_t dna_length = dna->length();

#ifdef BASE_4
  int8_t term_size = TERM_SIZE + Dna_7::polya_length;
#endif

  metadata_->rna_begin();
  for (int rna_idx = 0; rna_idx <
                        (int)metadata_->rna_count(); rna_idx++) {

    Rna_7* rna = metadata_->rna_next();

    if (rna->is_init_) {
      for (auto it_start_pos = rna->start_prot.begin(); it_start_pos != rna->start_prot.end(); it_start_pos++) {

        int start_prot = *it_start_pos;
        int start_protein_pos = rna->leading_lagging == 0 ?
                                                          start_prot +
                                                              PROT_START_SIZE :
                                                          start_prot -
                                                              PROT_START_SIZE;
        int length;

        if (rna->leading_lagging == 0) {
          start_protein_pos = start_protein_pos >= dna_length ?
                                                              start_protein_pos - dna_length
                                                              : start_protein_pos;

          if (start_prot < rna->end) {
            length = rna->end - start_prot;
          } else {
            length = dna_length -
                     start_prot +
                     rna->end;

          }

          length -= PROT_START_SIZE;
        } else {


          start_protein_pos = start_protein_pos < 0 ?
                                                    dna_length + start_protein_pos
                                                    : start_protein_pos;

          if (start_prot > rna->end) {
            length = (*it_start_pos) - rna->end;
          } else {
            length = *it_start_pos +
                     dna_length -
                     rna->end;
          }

          length -= PROT_START_SIZE;
        }

        bool is_protein = false;

        length += 1;
        length = length - (length % CODON_SIZE);

        int j = 0;
        int32_t transcribed_start = 0;

        if (rna->leading_lagging == 0) {
          transcribed_start = rna->begin + PROM_SIZE;
          transcribed_start = transcribed_start >= dna_length ?
                                                              transcribed_start - dna_length
                                                              : transcribed_start;

          if (transcribed_start <= start_prot) {
            j = start_prot - transcribed_start;
          } else {
            j = dna_length -
                transcribed_start +
                start_prot;

          }
        } else {
          transcribed_start = rna->begin - PROM_SIZE;
          transcribed_start = transcribed_start < 0 ?
                                                    dna_length + transcribed_start
                                                    : transcribed_start;

          if (transcribed_start >=
              start_prot) {
            j = transcribed_start -
                start_prot;
          } else {
            j = transcribed_start +
                dna_length - start_prot;
          }
        }

        j += PROT_START_SIZE;

        while (rna->length - j >= CODON_SIZE) {
          int t_k;

          if (rna->leading_lagging == 0) {
            start_protein_pos =
                start_protein_pos >= dna_length ?
                                                start_protein_pos - dna_length
                                                : start_protein_pos;

#ifdef BASE_2
            is_protein = false;

            for (int k = 0; k < 3; k++) {
              t_k = start_protein_pos + k;

              if (dna->get_lead(t_k) ==
                  PROTEIN_END_LEAD[k]) {
                is_protein = true;
              } else {
                is_protein = false;
                break;
              }
            }
#elif BASE_4
            is_protein = codon_value_to_aminoacid(bases_to_codon_value(
                             dna->get_lead(start_protein_pos),
                             dna->get_lead(start_protein_pos + 1),
                             dna->get_lead(start_protein_pos + 2)
                                 )) == STOP;
            t_k = start_protein_pos + CODON_SIZE;
#endif

            if (is_protein) {
              int prot_length = -1;

              if (start_prot + PROT_START_SIZE < t_k) {
                prot_length = t_k -
                              (start_prot +
                               PROT_START_SIZE);
              } else {
                prot_length = dna_length -
                              (start_prot +
                               PROT_START_SIZE) + t_k;
              }

              if (prot_length >= CODON_SIZE) {
                int32_t glob_prot_idx = -1;
                glob_prot_idx =
                    metadata_->proteins_count();
                metadata_->set_proteins_count(
                    metadata_->proteins_count() +
                    1);

#ifdef BASE_4
                int32_t prot_len = prot_length/CODON_SIZE;
                prot_len--;
                if (prot_len > 0) {
#endif

                  
                      metadata_->protein_add(glob_prot_idx, new Protein_7(
                                                                Utils::mod(start_prot+PROT_START_SIZE,dna_length), Utils::mod(t_k,dna_length),
#ifdef BASE_4
                                                                prot_len,
#else
                                                              prot_length/3,
#endif
                                                                rna->leading_lagging,
                                                                rna->e,rna
                                                                ));

                  rna->is_coding_ = true;
#ifdef BASE_4
                }
#endif
              }

              break;
            }

            start_protein_pos += CODON_SIZE;
            start_protein_pos =
                start_protein_pos >= dna_length ?
                                                start_protein_pos - dna_length
                                                : start_protein_pos;
          } else {


            start_protein_pos = start_protein_pos < 0 ?
                                                      dna_length + start_protein_pos
                                                      : start_protein_pos;

#ifdef BASE_2
            is_protein = false;
            for (int k = 0; k < 3; k++) {
              t_k = start_protein_pos - k;

              if (dna->get_lag(t_k) ==
                  PROTEIN_END_LAG[k]) {
                is_protein = true;
              } else {
                is_protein = false;
                break;
              }
            }
#elif BASE_4
            is_protein = codon_value_to_aminoacid(bases_to_codon_value(
                             dna->get_lag(start_protein_pos),
                             dna->get_lag(start_protein_pos - 1),
                             dna->get_lag(start_protein_pos - 2)
                                 )) == STOP;

            t_k = start_protein_pos - CODON_SIZE;
#endif

            if (is_protein) {
              int prot_length = -1;
              if (start_prot - PROT_START_SIZE > t_k) {
                prot_length =
                    (start_prot - PROT_START_SIZE) -
                    t_k;
              } else {
                prot_length =
                    (start_prot - PROT_START_SIZE) +
                    dna_length - t_k;
              }
              if (prot_length >= CODON_SIZE) {
                int32_t glob_prot_idx = -1;

                glob_prot_idx =
                    metadata_->proteins_count();
                metadata_->set_proteins_count(
                    metadata_->proteins_count() +
                    1);

#ifdef BASE_4
                int32_t prot_len = prot_length/CODON_SIZE;
                prot_len--;

                if (prot_len > 0) {
#endif

                  ((Metadata*)metadata_)->protein_add(
                      glob_prot_idx,
                      new Protein_7(Utils::mod(start_prot-PROT_START_SIZE,dna_length), Utils::mod(t_k,dna_length),
#ifdef BASE_4
                                    prot_len,
#else
                                  prot_length/3,
#endif
                                    rna->leading_lagging,
                                    rna->e,rna
                                    ));
                  rna->is_coding_ = true;
#ifdef BASE_4
                }
#endif
              }
              break;
            }
            start_protein_pos = start_protein_pos - CODON_SIZE;
            start_protein_pos = start_protein_pos < 0 ?
                                                      dna_length + start_protein_pos
                                                      : start_protein_pos;
          }
          j += CODON_SIZE;
        }
      }
    }
  }
}

void Individual_7::translate_protein(double w_max) {
  metadata_->protein_begin();
#ifdef BASE_4
  auto translate_protein = translate_protein_4B;
#else
  auto translate_protein = translate_protein_2B;
#endif
  for (int protein_idx = 0; protein_idx < (int)metadata_->proteins_count();
       protein_idx++) {
    Protein_7* prot = metadata_->protein_next();
    translate_protein(*prot, *dna_, w_max);
  }

  std::map<int32_t, Protein_7*> lookup;

  metadata_->protein_begin();

  for (int protein_idx = 0; protein_idx <
                            (int)metadata_->proteins_count(); protein_idx++) {
    {

      Protein_7* prot  =
          metadata_->protein_next();
      if (prot->is_init_ && prot->leading_lagging==0) {
        if (lookup.find(prot->protein_start) ==
            lookup.end()) {
          lookup[prot->protein_start] = prot;
        } else {
#ifdef __MULTI_PROMOTERS
  #if 0 == __MULTI_PROMOTERS
              if(prot->e > lookup[prot->protein_start]->e)
                lookup[prot->protein_start]->e = prot->e;
              if(prot->initial_e_ > lookup[prot->protein_start]->initial_e_)
                lookup[prot->protein_start]->initial_e_ = prot->initial_e_;
  #endif
#else
          lookup[prot->protein_start]->e += prot->e;
          lookup[prot->protein_start]->initial_e_ += prot->initial_e_;
#endif
          lookup[prot->protein_start]->rna_list_.insert(
              lookup[prot->protein_start]->rna_list_.end(),
              prot->rna_list_.begin(),prot->rna_list_.end());
          prot->is_init_ = false;
        }
      }
    }
  }

  lookup.clear();

  metadata_->protein_begin();

  for (int protein_idx = 0; protein_idx <
                            (int)metadata_->proteins_count(); protein_idx++) {
    {

      Protein_7* prot  =
          metadata_->protein_next();
      if (prot->is_init_ && prot->leading_lagging==1) {
        if (lookup.find(prot->protein_start) ==
            lookup.end()) {
          lookup[prot->protein_start] = prot;
        } else {
#ifdef __MULTI_PROMOTERS
  #if 0 == __MULTI_PROMOTERS
              if(prot->e > lookup[prot->protein_start]->e)
                lookup[prot->protein_start]->e = prot->e;
              if(prot->initial_e_ > lookup[prot->protein_start]->initial_e_)
                lookup[prot->protein_start]->initial_e_ = prot->initial_e_;
  #endif
#else
          lookup[prot->protein_start]->e += prot->e;
          lookup[prot->protein_start]->initial_e_ += prot->initial_e_;
#endif
          lookup[prot->protein_start]->rna_list_.insert(
              lookup[prot->protein_start]->rna_list_.end(),
              prot->rna_list_.begin(),prot->rna_list_.end());
          prot->is_init_ = false;
        }
      }
    }
  }


#ifdef __MULTI_PROMOTERS
  #if 1 == __MULTI_PROMOTERS
  metadata_->protein_begin();

  for (int protein_idx = 0; protein_idx <
                            (int)metadata_->proteins_count(); protein_idx++) {
    Protein_7* prot  =
        metadata_->protein_next();
    
    if (prot->is_init_) {
      Rna_7* first_rna = *(prot->rna_list_.begin());
      int32_t max_length = -1;

      if (prot->leading_lagging == 1) {
        max_length = prot->protein_start > first_rna->begin ? 
                                                            prot->protein_start - first_rna->begin : prot->protein_start + (dna_->length_ - first_rna->begin);
      } else {
        max_length = prot->protein_start < first_rna->begin ? 
                                                            first_rna->begin - prot->protein_start : first_rna->begin + (dna_->length_ - prot->protein_start);
      }

      for (auto rna : prot->rna_list_) {
        int32_t rna_length = -1;

        if (prot->leading_lagging == 1) {
          max_length = prot->protein_start > rna->begin ? 
                                                        prot->protein_start - rna->begin : prot->protein_start + (dna_->length_ - rna->begin);
        } else {
          max_length = prot->protein_start < rna->begin ? 
                                                        rna->begin - prot->protein_start : rna->begin + (dna_->length_ - prot->protein_start);
        }
      
        if (rna_length > max_length) {
          first_rna = rna;
          max_length = rna_length;
        }
      }

      prot->e = first_rna->e;
      prot->initial_e_ = first_rna->e;
    }

  }
  #endif
#endif

}

void Individual_7::compute_phenotype(FuzzyFactory_7* fuzzy_factory) {
  AbstractFuzzy_7* activ_phenotype = fuzzy_factory->create_fuzzy();
  AbstractFuzzy_7* inhib_phenotype = fuzzy_factory->create_fuzzy();


  std::vector<Protein_7*> protein_vector;
  metadata_->protein_begin();
  for (int protein_idx = 0; protein_idx < metadata_->proteins_count(); protein_idx++) {
    Protein_7* prot = metadata_->protein_next();
    if (prot->is_init_) {
      if (prot->leading_lagging==0)
        protein_vector.push_back(prot);
    }
  }

  metadata_->protein_begin();
  for (int protein_idx = 0; protein_idx < metadata_->proteins_count(); protein_idx++) {
    Protein_7* prot = metadata_->protein_next();
    if (prot->is_init_) {
      if (prot->leading_lagging==1)
        protein_vector.push_back(prot);
    }
  }

  std::sort(protein_vector.begin(), protein_vector.end(),
            [](Protein_7*a, Protein_7*b) { return *a < *b;});

  for (auto prot : protein_vector) {
    if (fabs(
            prot->w) >=
            1e-15 &&
        fabs(
            prot->h) >=
            1e-15) {

      if (prot->is_functional) {

        bool verbose = false;

        if (prot->h > 0)
          activ_phenotype->add_triangle(prot->m, prot->w, prot->h *
                                                              prot->e, verbose);
        else
          inhib_phenotype->add_triangle(prot->m, prot->w, prot->h *
                                                              prot->e, verbose);
      }
    }
  }

  activ_phenotype->clip(AbstractFuzzy_7::max,   Y_MAX);
  inhib_phenotype->clip(AbstractFuzzy_7::min, - Y_MAX);

  activ_phenotype->simplify();
  inhib_phenotype->simplify();

  if (phenotype != nullptr)
    phenotype->clear();
  else
    phenotype = fuzzy_factory->create_fuzzy();

  phenotype->copy(activ_phenotype);
  phenotype->add(inhib_phenotype);
  phenotype->clip(AbstractFuzzy_7::min, Y_MIN);
  phenotype->simplify();

  delete activ_phenotype;
  delete inhib_phenotype;

}

void Individual_7::compute_fitness(AbstractFuzzy_7* target,
                                   double selection_pressure,
                                   FuzzyFactory_7* fuzzy_factory) {
  AbstractFuzzy_7* delta = fuzzy_factory->create_fuzzy();

  delta->copy(phenotype);
  bool verbose = false;
  delta->sub(target,verbose);

  metaerror = delta->get_geometric_area();

  delete delta;

  fitness = exp(-selection_pressure * ((double)metaerror));
}

void Individual_7::evaluation_from_scratch(AbstractFuzzy_7* target,
                                           double w_max,
                                           double selection_pressure,
                                           FuzzyFactory_7* fuzzy_factory) {
  reset_metadata();
  start_stop_RNA();
  compute_RNA();
  start_protein();
  compute_protein();
  translate_protein(w_max);
  compute_phenotype(fuzzy_factory);
  compute_fitness(target, selection_pressure, fuzzy_factory);
}

void Individual_7::evaluate_after_mutation(AbstractFuzzy_7* target,
                                           double w_max,
                                           double selection_pressure,
                                           FuzzyFactory_7* fuzzy_factory) {

  opt_prom_compute_RNA();
  start_protein();
  compute_protein();
  translate_protein(w_max);
  compute_phenotype(fuzzy_factory);
  compute_fitness(target, selection_pressure, fuzzy_factory);
}
}
