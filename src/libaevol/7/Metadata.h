// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_METADATA_H
#define AEVOL_METADATA_H

#include "Dna_7.h"
#include "Individual_7.h"
#include "Protein_7.h"
#include "Rna_7.h"

#include <algorithm>

using std::list;

namespace aevol {

using Promoters1Strand_7 = std::list<PromoterStruct>;
using Promoters2Strands_7 = std::vector<Promoters1Strand_7>;

class Metadata {
 public:
  Metadata(Individual_7* indiv, Metadata* metadata);

  Metadata(Individual_7* indiv,
                int32_t* lead_prom_pos = nullptr,
                int8_t* lead_prom_error = nullptr,
                int32_t lead_prom_size = -1,
                int32_t* lag_prom_pos = nullptr,
                int8_t* lag_prom_error = nullptr,
                int32_t lag_prom_size = -1);

  ~Metadata();

  void clean_remote();

#ifdef __REGUL
  void add_inherited_proteins(Individual_7* indiv = nullptr);
#endif
  /** Getter **/

  void set_iterators() {
    it_promoter_ = promoters_list_[LEADING].begin();
    it_rna_ = rnas_.begin();
    it_protein_ = proteins_.begin();
  }

  /*** Promoters ***/
  int8_t is_promoter_leading(int pos);

  int8_t is_promoter_lagging(int pos);
  PromoterStruct* promoters(int idx);
  void promoter_add(int idx, PromoterStruct* prom);

  PromoterStruct* promoter_next();
  void promoter_begin();
  bool promoter_end();

  int promoter_count();
  void set_promoters_count(int pcount);

  /*** Terminators ***/
#ifdef BASE_4
  bool is_terminator_leading(int pos);

  bool is_terminator_lagging(int pos);
#endif
  int terminator_count(int LoL);
  void terminator_add(int LoL, int dna_pos);

  int next_terminator(int LoL, int dna_pos);

  void terminators_clear();

  /*** RNAs ***/
  Rna_7* rnas(int idx);
  void rna_add(int idx, Rna_7* rna, PromoterStruct* prom = nullptr);
  void rna_add(int idx,
               int32_t t_begin,
               int32_t t_end,
               int8_t t_leading_lagging,
               double t_e,
               int32_t t_length,
               PromoterStruct* prom = nullptr);

  Rna_7* rna_next();
  void rna_begin();
  bool rna_end();

  int rna_count();
  void set_rna_count(int rcount);

  void rnas_resize(int resize);
  void rnas_clear();

  /*** Shine Dal + Start Codon ***/
  int8_t is_shine_dal_start_prot_leading(int pos);

  int8_t is_shine_dal_start_prot_lagging(int pos);

  /*** Proteins ***/
  Protein_7* proteins(int idx);
  void protein_add(int idx, Protein_7* prot);
  void protein_add(int idx,
                   int32_t t_protein_start,
                   int32_t t_protein_end,
                   int32_t t_protein_length,
                   int8_t t_leading_lagging,
                   double t_e,
                   Rna_7* rna);

  Protein_7* protein_next();
  void protein_begin();
  bool protein_end();

  int proteins_count();
  void set_proteins_count(int pcount);

  void proteins_resize(int resize);
  void proteins_clear();

  void proteins_remove_signal();

  void proteins_print(int step = -1, int indiv_id = -1);

  /*** Promoters ***/
  void lst_promoters(
      bool lorl,
      Position
          before_after_btw,  // with regard to the strand's reading direction
      int32_t pos1,
      int32_t pos2,
      std::list<PromoterStruct*>& motif_list);

  /** Search and update **/
  void remove_promoters_around(int32_t pos_1);
  void remove_promoters_around(int32_t pos_1, int32_t pos_2);
  void remove_all_promoters();

  void look_for_new_promoters_around(int32_t pos_1, int32_t pos_2);
  void look_for_new_promoters_around(int32_t pos);

  void locate_promoters();

  void move_all_promoters_after(int32_t pos, int32_t delta_pos);

  void duplicate_promoters_included_in(
      int32_t pos_1,
      int32_t pos_2,
      std::vector<std::list<PromoterStruct*>>& duplicated_promoters);
  void extract_promoters_included_in(
      int32_t pos_1,
      int32_t pos_2,
      std::vector<std::list<PromoterStruct*>>& extracted_promoters);
  void insert_promoters(
      std::vector<std::list<PromoterStruct*>>& promoters_to_insert);
  void insert_promoters_at(
      std::vector<std::list<PromoterStruct*>>& promoters_to_insert,
      int32_t pos);

  void invert_promoters_included_in(int32_t pos1, int32_t pos2);

  static void
  shift_promoters(std::vector<std::list<PromoterStruct*>>& promoters_to_shift,
                  int32_t delta_pos,
                  int32_t seq_length);
  static void
  invert_promoters(std::vector<std::list<PromoterStruct*>>& promoter_lists,
                   int32_t pos1,
                   int32_t pos2);

  void remove_leading_promoters_starting_between(int32_t pos_1, int32_t pos_2);
  void remove_leading_promoters_starting_after(int32_t pos);
  void remove_leading_promoters_starting_before(int32_t pos);

  void remove_lagging_promoters_starting_between(int32_t pos_1, int32_t pos_2);
  void remove_lagging_promoters_starting_after(int32_t pos);
  void remove_lagging_promoters_starting_before(int32_t pos);

  void move_all_leading_promoters_after(int32_t pos, int32_t delta_pos);
  void move_all_lagging_promoters_after(int32_t pos, int32_t delta_pos);

  void look_for_new_leading_promoters_starting_between(int32_t pos_1,
                                                       int32_t pos_2);
  void look_for_new_leading_promoters_starting_after(int32_t pos);
  void look_for_new_leading_promoters_starting_before(int32_t pos);

  void look_for_new_lagging_promoters_starting_between(int32_t pos_1,
                                                       int32_t pos_2);
  void look_for_new_lagging_promoters_starting_after(int32_t pos);
  void look_for_new_lagging_promoters_starting_before(int32_t pos);

  void promoters_included_in(
      int32_t pos_1,
      int32_t pos_2,
      std::vector<std::list<PromoterStruct*>>& promoters_list);

  void extract_leading_promoters_starting_between(
      int32_t pos_1,
      int32_t pos_2,
      std::list<PromoterStruct*>& extracted_promoters);

  void extract_lagging_promoters_starting_between(
      int32_t pos_1,
      int32_t pos_2,
      std::list<PromoterStruct*>& extracted_promoters);

  int32_t length() { return indiv_->dna_->length(); }

#ifdef WITH_OPTIMIZE_DIFF_SEARCH

  void reinit_rnas(int32_t pos);

  void reinit_rnas(int32_t pos_1, int32_t pos_2) {
    // if (indiv_->indiv_id==6)
    //     printf("Init RNA %d %d\n",pos_1,pos_2);
    for (auto i = pos_1; i <= pos_2; i++) {
      reinit_rnas(i);
      reinit_rnas(i);
    }
  }
#endif

  Promoters2Strands_7 promoters_list_ = {{}, {}};

  std::list<Protein_7*> proteins_;

  std::list<Rna_7*> rnas_;

  int32_t rna_count_ = 0;
  Individual_7* indiv_;

 protected:
  Promoters1Strand_7::iterator it_promoter_;
  int it_promoter_pos_;

  std::list<Rna_7*>::iterator it_rna_;
  std::list<Protein_7*>::iterator it_protein_;

  std::set<int> terminator_lag_;
  std::set<int> terminator_lead_;

  int32_t protein_count_ = 0;
  int32_t rna_count_add_ = 0;

  int cmp_rna = 0;
};
}  // namespace aevol

#endif  //AEVOL_METADATA_H
