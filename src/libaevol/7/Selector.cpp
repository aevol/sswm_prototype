//
// Created by elturpin on 02/06/22.
//
#include "Selector.h"

using namespace aevol;

void GlobalSelector::operator()(const IndividualArray& population,
                                std::vector<int>& reproducers_location,
                                UPrngArray& prngs) const {
#pragma omp single
  {
    // Only use one prng
    auto& prng = prngs[0];
    double* local_fit_array = new double[grid_size_];
    double* probs = new double[grid_size_];
    double sum_local_fit = 0.0;

    for (int location = 0; location < grid_size_; location++) {
      local_fit_array[location] = population[location]->fitness;
      sum_local_fit += local_fit_array[location];
    }

    for (int location = 0; location < grid_size_; location++) {
      probs[location] = local_fit_array[location] / sum_local_fit;
    }

    auto* nb_offsprings = new int[grid_size_];
    prng->multinomial_drawing(nb_offsprings, probs, grid_size_, grid_size_);

    int dest_location = 0;

    for (int32_t location = 0; location < grid_size_; location++) {
      for (int32_t j = 0; j < nb_offsprings[location]; j++) {
        reproducers_location[dest_location] = location;
        dest_location++;
      }
    }

    delete[] nb_offsprings;
    delete[] local_fit_array;
    delete[] probs;
  }
}

void LocalSelector::select_one_location(int location,
                                        int& location_of_reproducer,
                                        const IndividualArray& population,
                                        JumpingMT& prng) const {
  int neighborhood_size = selection_scope_x_ * selection_scope_y_;

  double* local_fit_array = new double[neighborhood_size];
  double* local_meta_array = new double[neighborhood_size];
  double* probs = new double[neighborhood_size];
  int count = 0;
  double sum_local_fit = 0.0;

  int x = location / grid_height_;
  int y = location % grid_height_;
  int cur_x, cur_y;

  int tab_id = 0;

  for (int i = -(selection_scope_x_ / 2); i <= (selection_scope_x_ / 2); i++) {
    for (int j = -(selection_scope_y_ / 2); j <= (selection_scope_y_ / 2);
         j++) {
      cur_x = (x + i + grid_width_) % grid_width_;
      cur_y = (y + j + grid_height_) % grid_height_;

      local_fit_array[count] =
          population[cur_x * grid_height_ + cur_y]->fitness;

      sum_local_fit += local_fit_array[count];

      count++;
      tab_id++;
    }
  }

  for (int i = 0; i < neighborhood_size; i++) {
    probs[i] = local_fit_array[i] / sum_local_fit;
  }

  int16_t found_org = prng.roulette_random(probs, neighborhood_size);

  int32_t x_offset, y_offset;

  x_offset = (found_org / selection_scope_x_) - 1;
  y_offset = (found_org % selection_scope_x_) - 1;

  delete[] local_fit_array;
  delete[] local_meta_array;
  delete[] probs;

  location_of_reproducer =
      ((x + x_offset + grid_width_) % grid_width_) * grid_height_ +
      ((y + y_offset + grid_height_) % grid_height_);
}

void LocalSelector::operator()(const IndividualArray& population,
                               std::vector<int>& reproducers_location,
                               UPrngArray& prngs) const {
  auto grid_size = grid_height_ * grid_width_;
#pragma omp for
  for (int i = 0; i < grid_size; ++i) {
    select_one_location(i, reproducers_location[i], population, *(prngs[i]));
  }
}
