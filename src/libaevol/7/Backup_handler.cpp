//
// Created by elturpin on 18/05/22.
//

#include "Backup_handler.h"

#include <map>
#include <zlib.h>

using namespace aevol;

void bu::write_backup(int generation,
                      const IndividualArray& population,
                      const UPrngArray& reprod_prngs,
                      const SPrngArray& mut_prngs) {
  // Open the right file
  // todo: find correct naming
  char name[50];
  sprintf(name, "module_backup_%d.bin", generation);
  auto backup_file = gzopen(name, "w");

  // Save PRNGs state
  for (const auto& prng: reprod_prngs) {
    prng->save(backup_file);
  }

  for (const auto& prng: mut_prngs) {
    prng->save(backup_file);
  }

  // Save population
  std::map<const Individual_7*, std::vector<int>> population_map;
  for (auto loc_id = 0UL; loc_id < population.size(); ++loc_id) {
    population_map[population[loc_id].get()].push_back(loc_id);
  }

  int nb_unique_indiv = (int)population_map.size();
  gzwrite(backup_file, &nb_unique_indiv, sizeof(nb_unique_indiv));
  for (const auto& [indiv_ptr, locations]: population_map) {
    indiv_ptr->save_to_backup(backup_file);
    int loc_number = locations.size();
    gzwrite(backup_file, &loc_number, sizeof(loc_number));
    gzwrite(backup_file, locations.data(), loc_number * sizeof(int));
  }

  gzclose(backup_file);
}

IndividualArray bu::read_backup(int generation,
                                IndividualArray& population,
                                UPrngArray& reprod_prngs,
                                SPrngArray& mut_prngs) {
  // Open the right file
  // todo: find correct naming
  char name[50];
  sprintf(name, "module_backup_%d.bin", generation);
  auto backup_file = gzopen(name, "r");
  if (backup_file == Z_NULL) {
    fprintf(stderr, "The file %s cannot be opened\n", name);
    exit(EXIT_FAILURE);
  }

  printf("Will read backup at file: %s\n", name);

  // Load PRNGs state
  for (auto& prng: reprod_prngs) {
    prng = std::make_unique<JumpingMT>(backup_file);
  }

  for (auto& prng: mut_prngs) {
    prng = std::make_shared<JumpingMT>(backup_file);
  }

  // Load Population
  int nb_unique_indiv;
  gzread(backup_file, &nb_unique_indiv, sizeof(nb_unique_indiv));

  IndividualArray unique_indivs;
  for (int i = 0; i < nb_unique_indiv; ++i) {
    auto new_indiv = std::make_shared<Individual_7>(0.0);
    new_indiv->load_from_backup(backup_file);
    unique_indivs.push_back(new_indiv);
    int loc_number;
    gzread(backup_file, &loc_number, sizeof(loc_number));
    auto* locations = new int[loc_number];
    gzread(backup_file, locations, loc_number * sizeof(int));
    for (int j = 0; j < loc_number; ++j) {
      population[locations[j]] = new_indiv;
    }
    delete[] locations;
  }
  gzclose(backup_file);
  return unique_indivs;
}