// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "ExpManager_7.h"

#include "AbstractFuzzy_7.h"
#include "AeTime.h"
#include "Metadata.h"
#include "Promoter.h"
#include "Protein_7.h"
#include "Rna_7.h"
#include "MutationDataAdapter.h"

#include <algorithm>
#include <chrono>
#include <err.h>
#include <fstream>
#include <sys/stat.h>

namespace aevol {

#define __VECTORIZE_STRCMP

void init_prng(std::vector<std::unique_ptr<JumpingMT>>& reprod_prng,
               std::vector<std::shared_ptr<JumpingMT>>& mut_prng,
               int seed) {
  const int RANGE = 1000000;
  JumpingMT tmp_prng(seed);
  for (auto& prng: reprod_prng) {
    prng = std::make_unique<JumpingMT>(tmp_prng.random(RANGE));
  }
  for (auto& prng: mut_prng) {
    prng = std::make_shared<JumpingMT>(tmp_prng.random(RANGE));
  }
}

ExpManager_7::ExpManager_7(double width,
                           double height,
                           int backup_step,
                           int tree_step,
                           MutationParameters mut_parameters,
                           FuzzyFactory_7* factory,
                           AbstractFuzzy_7* src_target,
                           std::unique_ptr<Selector>&& selection) {
  selector = std::move(selection);

  grid_height_ = height;
  grid_width_ = width;
  nb_indivs_ = grid_height_ * grid_width_;

  selection_scope_x_ = selection_scope_y_ = 3;
  fitness_function_ = FITNESS_EXP;

  backup_step_ = backup_step;

  fuzzy_factory_ = factory;
  target = src_target;

  if (tree_step > 0) {
    tree_ = new Tree(nb_indivs_, tree_step);
  }

  mutation_parameters_ = mut_parameters;

  current_individuals.resize(nb_indivs_, nullptr);
  previous_individuals.resize(nb_indivs_, nullptr);

  dna_mutator_array_.resize(nb_indivs_, nullptr);
  next_generation_reproducer_.resize(nb_indivs_);

  reprod_prng_.resize(nb_indivs_);
  mut_prng_.resize(nb_indivs_);

  stats_best = new Stats_7(AeTime::time(), true);
}

void ExpManager_7::selection(int indiv_id) {
  int16_t neighborhood_size = selection_scope_x_ * selection_scope_y_;

  double* local_fit_array = new double[neighborhood_size];
  double* local_meta_array = new double[neighborhood_size];
  double* probs = new double[neighborhood_size];
  int16_t count = 0;
  double sum_local_fit = 0.0;

  int32_t x = indiv_id / grid_height_;
  int32_t y = indiv_id % grid_height_;

  int cur_x, cur_y;
  int tab_id = 0;

  for (int8_t i = -(selection_scope_x_ / 2); i <= (selection_scope_x_ / 2);
       i++) {
    for (int8_t j = -(selection_scope_y_ / 2); j <= (selection_scope_y_ / 2);
         j++) {
      cur_x = (x + i + grid_width_) % grid_width_;
      cur_y = (y + j + grid_height_) % grid_height_;

      local_fit_array[count] =
          previous_individuals[cur_x * grid_height_ + cur_y]->fitness;

      sum_local_fit += local_fit_array[count];

      count++;
      tab_id++;
    }
  }

  for (int16_t i = 0; i < neighborhood_size; i++) {
    probs[i] = local_fit_array[i] / sum_local_fit;
  }

  int16_t found_org =
      reprod_prng_[indiv_id]->roulette_random(probs, neighborhood_size);
  int32_t x_offset, y_offset;

  x_offset = (found_org / selection_scope_x_) - 1;
  y_offset = (found_org % selection_scope_x_) - 1;

  delete[] local_fit_array;
  delete[] local_meta_array;
  delete[] probs;

  next_generation_reproducer_[indiv_id] =
      ((x + x_offset + grid_width_) % grid_width_) * grid_height_ +
      ((y + y_offset + grid_height_) % grid_height_);
}

void ExpManager_7::do_mutation(int indiv_id) {
  int32_t dna_length;

  int32_t x = indiv_id / grid_height_;
  int32_t y = indiv_id % grid_height_;
  delete dna_mutator_array_[indiv_id];

  dna_length = previous_individuals[next_generation_reproducer_[indiv_id]]->dna_->length();

  dna_mutator_array_[indiv_id] = new DnaMutator(
      mut_prng_[indiv_id],
      dna_length,
      mutation_parameters_.duplication_rate_,
      mutation_parameters_.deletion_rate_,
      mutation_parameters_.translocation_rate_,
      mutation_parameters_.inversion_rate_,
      mutation_parameters_.point_mutation_rate_,
      mutation_parameters_.small_insertion_rate_,
      mutation_parameters_.small_deletion_rate_,
      mutation_parameters_.max_indel_size_,
      mutation_parameters_.min_genome_length_,
      mutation_parameters_.max_genome_length_,
      indiv_id,
      x,y
      );
  dna_mutator_array_[indiv_id]->generate_mutations();

  if (dna_mutator_array_[indiv_id]->hasMutate()) {
    dna_mutator_array_[indiv_id]->generate_all_mutations(dna_mutator_array_[indiv_id]->length_);

    #pragma omp critical
    {
    mutant_list_.push_back(indiv_id);
    }
  } else {

    int32_t parent_id = next_generation_reproducer_[indiv_id];
    current_individuals[indiv_id] = previous_individuals[parent_id];

    if (tree_ != nullptr) {
      NewIndivEvent *eindiv = new NewIndivEvent(
            current_individuals[indiv_id].get(),
            previous_individuals[next_generation_reproducer_[indiv_id]].get(),
            x, y, indiv_id,
            next_generation_reproducer_[indiv_id]);
      tree_->update_new_indiv(eindiv);
      delete eindiv;
    }
  }
}

void record_mutations_to_tree(int indiv_id,
                              std::list<MutationEvent*>& mutation_list,
                              Tree* tree) {
  for (auto* event: mutation_list) {
    auto* mut = MutationDataAdapter::mutation_event_to_mutation(*event);
    assert(mut != nullptr);
    tree->report_by_index(AeTime::time(), indiv_id)
        ->dna_replic_report()
        .add_mut(mut);
    delete mut;
  }
}

void ExpManager_7::apply_mutations(int indiv_id) {
  current_individuals[indiv_id] = std::make_shared<Individual_7>(
      previous_individuals[next_generation_reproducer_[indiv_id]].get());

  if (tree_ != nullptr) {
      int x = indiv_id / grid_height_;
      int y = indiv_id % grid_height_;

      NewIndivEvent *eindiv = new NewIndivEvent(
            current_individuals[indiv_id].get(),
            previous_individuals[next_generation_reproducer_[indiv_id]].get(),
            x, y, indiv_id,
            next_generation_reproducer_[indiv_id]);

      tree_->update_new_indiv(eindiv);
      delete eindiv;
    }
((Metadata*)current_individuals[indiv_id]->metadata_)->indiv_ =
        current_individuals[indiv_id].get();

  auto& mutation_list = dna_mutator_array_[indiv_id]->mutation_list_;

  current_individuals[indiv_id]->dna_->apply_mutations(mutation_list);

  if (tree_ != nullptr)
    record_mutations_to_tree(indiv_id, mutation_list, tree_);
}

ExpManager_7::~ExpManager_7() {

  printf("Destroy SIMD Controller\n");

  delete stats_best;
  delete stats_mean;

  for (int indiv_id = 0; indiv_id < (int) nb_indivs_; indiv_id++) {
    delete dna_mutator_array_[indiv_id];
  }

  delete target;
  delete fuzzy_factory_;
  delete tree_;
}

void ExpManager_7::run_a_step(double w_max, double selection_pressure) {

#pragma omp single
  {
    mutant_list_.clear();
    mutant_list_.reserve(nb_indivs_);
  }

(*selector)(previous_individuals, next_generation_reproducer_, reprod_prng_);

#pragma omp for schedule(dynamic)
  for (int indiv_id = 0; indiv_id < nb_indivs_; indiv_id++) {
    do_mutation(indiv_id);
  }

#ifdef __GNUC__
#pragma omp for schedule(dynamic,1)
#else
#pragma omp for schedule(monotonic:dynamic,1)
#endif
for (int index = 0; index < mutant_list_.size(); index++) {
  int32_t indiv_id = mutant_list_[index];

  if (dna_mutator_array_[indiv_id]->hasMutate()) {
    apply_mutations(indiv_id);
    auto& individual = current_individuals[indiv_id];
    individual->evaluate_after_mutation(target, w_max, selection_pressure,
                                        fuzzy_factory_);
  }
}

#pragma omp for schedule(dynamic)
  for (int indiv_id = 0; indiv_id < nb_indivs_; indiv_id++) {
    current_individuals[indiv_id]->is_at_border_ = false;
    if (tree_ != nullptr) {
      int x = indiv_id / grid_height_;
      int y = indiv_id % grid_height_;
      auto* eindiv =
          new EndReplicationEvent(current_individuals[indiv_id].get(), x, y,
                                  indiv_id);
      // Tell observers the replication is finished
      tree_->update_end_replication(eindiv);
      delete eindiv;
    }
  }

#pragma omp for schedule(static)
  for (int indiv_id = 0; indiv_id < (int)nb_indivs_; indiv_id++) {
    previous_individuals[indiv_id] = current_individuals[indiv_id];
    current_individuals[indiv_id] = nullptr;
  }
#pragma omp single
  {
    // Todo: find why calling this function change the result of the simulation
//    write_stat();
    if (tree_ && AeTime::time() % tree_->tree_step() == 0) {
      int status;
      status = mkdir(TREE_DIR, 0755);
      if ((status == -1) && (errno != EEXIST)) {
        err(EXIT_FAILURE, "Impossible to create the directory %s", TREE_DIR);
      }

      char tree_file_name[50];
      sprintf(tree_file_name, "tree/tree_" TIMESTEP_FORMAT ".ae",
              AeTime::time());
      std::cout << "writing 7777 tree for gen : " << AeTime::time() << '\n';
      tree_->write_to_tree_file(tree_file_name);
    }
  }
}

void ExpManager_7::write_stat(bool just_best, bool non_coding) {
  stats_best->reinit(AeTime::time());

  // Search for the best
  double best_fitness = previous_individuals[0]->fitness;
  int idx_best = 0;
  for (int indiv_id = 1; indiv_id < (int) nb_indivs_; indiv_id++) {
    if (previous_individuals[indiv_id]->fitness > best_fitness) {
      idx_best = indiv_id;
      best_fitness = previous_individuals[indiv_id]->fitness;
  
    }
  }
  best_indiv = previous_individuals[idx_best];
  best_indiv->reset_stats();
    best_indiv->metadata_->rna_begin();
    
    // ANNOTATE_SITE_BEGIN(mainloop);
    for (int prom_idx = 0;
         prom_idx < (int)best_indiv->metadata_->rna_count(); prom_idx++) {
          //  ANNOTATE_ITERATION_TASK(searchMain);
      Rna_7* rna =
          best_indiv->metadata_->rna_next();
    if (rna != nullptr) {
      if (rna->is_coding_)
        best_indiv->nb_coding_RNAs++;
      else
        best_indiv->nb_non_coding_RNAs++;
    }
  }


  for (int i = 0; i < best_indiv->metadata_->proteins_count(); i++) {
    Protein_7*prot = best_indiv->metadata_->proteins(i);
    if (prot != nullptr) {
      if (prot->is_functional) {
        best_indiv->nb_func_genes++;
      } else {
        best_indiv->nb_non_func_genes++;
      }
      if (prot->h > 0) {
        best_indiv->nb_genes_activ++;
      } else {
        best_indiv->nb_genes_inhib++;
      }
    }
  }

  stats_best->write(*best_indiv,non_coding);
}
}
