// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************




// =================================================================
//                              Includes
// =================================================================
#include "Mutation.h"

#include "Deletion.h"
#include "Duplication.h"
#include "Inversion.h"
#include "PointMutation.h"
#include "SmallDeletion.h"
#include "SmallInsertion.h"
#include "Translocation.h"
#include "Utils.h"

#include <cstdlib>

namespace aevol {


//#############################################################################
//
//                                Class Mutation
//
//#############################################################################

// =================================================================
//                    Definition of static attributes
// =================================================================

// =================================================================
//                             Constructors
// =================================================================
Mutation* Mutation::Load(gzFile backup_file) {
  // Retrieve mutation type
  int8_t tmp_mut_type;
  gzread(backup_file, &tmp_mut_type,  sizeof(tmp_mut_type));
  MutationType mut_type = (MutationType) tmp_mut_type;

  // Call the appropriate constructor accordingly
  Mutation* mut;


      // printf("Mut type %d\n",mut_type);

  switch (mut_type) {
    case SWITCH :
      mut = new PointMutation();
      break;
    case S_INS :
      mut = new SmallInsertion();
      break;
    case S_DEL :
      mut = new SmallDeletion();
      break;
    case DUPL :
      mut = new Duplication();
      break;
    case DEL :
      mut = new Deletion();
      break;
    case TRANS :
      mut = new Translocation();
      break;
    case INV :
      mut = new Inversion();
      break;
    default :
      Utils::ExitWithDevMsg("invalid mutation type ", __FILE__, __LINE__);
      exit(-1); // Superfluous but suppresses a warning
  }

  // Load from backup file
  mut->load(backup_file);
  return mut;
}

// =================================================================
//                             Destructor
// =================================================================

// =================================================================
//                            Public Methods
// =================================================================

// =================================================================
//                           Protected Methods
// =================================================================
} // namespace aevol
