// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

// ============================================================================
//                                   Includes
// ============================================================================
#include "AeTime.h"
#include "json.hpp"
#include "macros.h"
#include "Tree.h"
#include "Utils.h"

#include <cinttypes>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <getopt.h>
#include <list>
#include <zlib.h>

enum check_type
{
    FULL_CHECK  = 0,
    LIGHT_CHECK = 1,
    NO_CHECK    = 2
};

using namespace aevol;
using json = nlohmann::json;

// Helper functions
void print_help(char* prog_path);
void interpret_cmd_line_options(int argc, char* argv[]);

// Command-line option variables
static bool best = true;
static bool verbose = true; // Todo: turn it into a program parameter
static int64_t t0 = 0;
static int64_t t_end = -1;
static int32_t final_indiv_index = -1;
static char tree_file_name[255]; // TODO remove magic number
static char* input_file_name = nullptr;

struct Parameters {
  int backup_step;
  int tree_step;
  int width;
  int height;
};

void from_json(const json& j, Parameters& p) {
  assert(j.at("individual").is_string());
  j.at("backup_step").get_to(p.backup_step);
  j.at("tree_step").get_to(p.tree_step);
  j.at("grid_dimension")[0].get_to(p.width);
  j.at("grid_dimension")[1].get_to(p.height);
}

int main(int argc, char** argv) {
  // The output file (lineage.ae) contains the following information:
  // You may check that this information is up-to-date by searching
  // "lineage_file" in this source file
  //
  // - t0
  // - t_end
  // - final individual index
  // - final individual rank
  // - initial_ancestor information (including its genome)
  // - replication report of ancestor at generation t0+1
  // - replication report of ancestor at generation t0+2
  // - replication report of ancestor at generation t0+3
  // - ...
  // - replication report of ancestor at generation t_end

  interpret_cmd_line_options(argc, argv);
  std::ifstream input_file(input_file_name);

  json inputs;
  input_file >> inputs;
  input_file.close();

  Parameters p = inputs.get<Parameters>();

  // The tree
  Tree* tree = nullptr;
  long tree_step = p.tree_step;
  auto nb_indivs = p.height * p.width;

  // Indices, ranks and replication reports of the individuals in the lineage
  int32_t* indices = new int32_t[t_end - t0 + 1];
  ReplicationReport** reports = new ReplicationReport*[t_end - t0];
  // NB: we do not need the report of the ancestor at time t0 since we have
  // retrieved the individual itself from the initial backup
  // (plus it might be the generation 0, for which we have no reports)
  // reports[0] = how ancestor at t0 + 1 was created
  // reports[i] = how ancestor at t0 + i + 1 was created
  // reports[t_end - t0 - 1] = how the final individual was created
  //
  //           ---------------------------------------------------------------
  //  reports |  t0 => t1   |  t1 => t2   |...| t_n-1 => t_n   | XXXXXXXXXXXX |
  //           ---------------------------------------------------------------
  //  indices | index at t0 | index at t1 |...| index at t_n-1 | index at t_n |
  //           ---------------------------------------------------------------



  // =========================
  //  Load the last tree file
  // =========================

  // Example for ae_common::rec_params->tree_step() == 100 :
  //
  // tree_000100.ae ==>  timesteps 1 to 100.
  // tree_000200.ae ==>  timesteps 101 to 200.
  // tree_000300.ae ==>  timesteps 201 to 300.
  // etc.
  //

  sprintf(tree_file_name,"tree/tree_" TIMESTEP_FORMAT ".ae", t_end);

  printf("Loading %ld\n",t_end);

  tree = new Tree(tree_file_name, nb_indivs, p.tree_step);

  if (verbose) {
    printf("OK\n");
    printf("====================================\n");
  }


  // ============================================================================
  //  Find the index of the final individual and retrieve its replication report
  // ============================================================================
  if (best) {
    // Todo: Find a way to find best indiv of population from backup
    int best_id = 000;
    reports[t_end - t0 - 1] =
        new ReplicationReport(*(tree->report_by_index(t_end, best_id)));
    // final_indiv_rank = reports[t_end - t0 - 1]->rank();

    indices[t_end - t0]  = best_id;
  } else {
    assert(final_indiv_index != -1);
    // The index was directly provided, get the replication report and update the indices and ranks tables
    reports[t_end - t0 - 1] =
        new ReplicationReport(*(tree->report_by_index(t_end,
                                                      final_indiv_index)));
    // final_indiv_rank = reports[t_end - t0 - 1]->rank();

    indices[t_end - t0]  = final_indiv_index;
  }

  if (verbose) {
    printf("The final individual has index %" PRId32
           "\n", final_indiv_index);
  }

  // =======================
  //  Open the output file
  // =======================
  char output_file_name[101];

    snprintf(output_file_name, 100,
        "lineage-b" TIMESTEP_FORMAT "-e" TIMESTEP_FORMAT "-i%" PRId32 ".ae",
        t0, t_end, final_indiv_index);

  gzFile lineage_file = gzopen(output_file_name, "w");
  if (lineage_file == nullptr) {
    fprintf(stderr, "File %s could not be created.\n", output_file_name);
    fprintf(stderr, "Please check your permissions in this directory.\n");
    exit(EXIT_FAILURE);
  }

  // ===================================================
  //  Retrieve the replication reports of the ancestors
  // ===================================================

  if (verbose) {
    printf("\n\n\n");
    printf("======================================================================\n");
    printf(" Parsing tree files to retrieve the ancestors' replication reports... \n");
    printf("======================================================================\n");
  }

  // Retrieve the index of the first ancestor from the last replication report
  indices[t_end - t0 -1] = reports[t_end - t0 - 1]->parent_id();

  // For each generation (going backwards), retrieve the index of the parent and
  // the corresponding replication report
  for (int64_t i = t_end - t0 - 2 ; i >= 0 ; i--) {
    int64_t t = t0 + i + 1;

    // We want to fill reports[i], that is to say, how the ancestor
    // at generation begin_gener + i + 1  was created
    if (verbose)
      printf("Getting the replication report for the ancestor at generation %" PRId64 " : %d\n", t,indices[i+1]);

    // If we've exhausted the current tree file, load the next one
    if (Utils::mod(t, tree_step) == 0) {
      // Change the tree file
      delete tree;
      sprintf(tree_file_name,"tree/tree_" TIMESTEP_FORMAT ".ae", t);
      if (Utils::mod(t, tree_step) == 0)
        AeTime::set_time(AeTime::time()-tree_step);


      // printf("Loading another ExpManager (Switch from rank %d to %d\n",current_rank,owner_rank);
      //current_rank = owner_rank;
      tree = new Tree(tree_file_name, nb_indivs, tree_step);
    }

    // Copy the replication report of the ancestor
    reports[i] =
        new ReplicationReport(*(tree->report_by_index(t,indices[i + 1])));
    // printf("Report for %d is %d => %d -- I %d\n",reports[i]->parent_id(),reports[i]->id(),indices[i+1]);

    // Retreive the index and rank of the next ancestor from the report
    indices[i] = reports[i]->parent_id();
  }

  delete tree; // delete the last Tree

  if (verbose) printf("OK\n");

  // =============================================================================
  //  Get the initial genome from the backup file and write it in the output file
  // =============================================================================

  if (verbose) {
    printf("\n\n\n");
    printf("=============================================== \n");
    printf(" Getting the initial genome sequence... ");
    fflush(nullptr);
  }

  // Write file "header"
  gzwrite(lineage_file, &t0, sizeof(t0));
  gzwrite(lineage_file, &t_end, sizeof(t_end));

  // Copy the initial ancestor
  // TODO: the initial ancestor is not included in the file because it is
  //  already present in the initial json file. Following post treatments
  //  will read the genome from this json


  if (verbose) {
    printf("OK\n");
    printf("=============================================== \n");
  }


  // ===============================================================================
  //  Write the replication reports of the successive ancestors in the output file
  //  (and, optionally, check that the rebuilt genome is correct each time a backup
  //  is available)
  // ===============================================================================

  if (verbose) {
    printf("\n\n\n");
    printf("============================================================ \n");
    printf(" Write the replication reports in the output file... \n");
    printf("============================================================ \n");
  }

  std::ofstream of;
  of.open("lignée_old.txt");

  for (int64_t i = 0 ; i < t_end - t0 ; i++) {
    // Where are we in time...
    int64_t t = t0 + i + 1;

    // Write the replication report of the ancestor for current generation
    if (verbose) {
      printf("Writing the replication report for t= %" PRId64
             " (built from indiv %" PRId32 " %" PRId32" at t= %" PRId64 ") "
             "%lld => %lld\n",
             t, indices[i], indices[i+1], t-1, reports[i]->parent_id(), reports[i]->id());
    }
    of << t << " : " << reports[i]->parent_id() << "  " << reports[i]->id() << std::endl;

    reports[i]->write_to_tree_file(lineage_file);

    if (verbose) printf(" OK\n");

  }

  gzclose(lineage_file);
  of.close();
  for (int64_t i_rep = 0 ; i_rep < t_end - t0 ; i_rep++) {
    delete reports[i_rep];
  }
  delete [] reports;
  delete [] indices;

  exit(EXIT_SUCCESS);
}

/**
 * \brief print help and exist
 */
void print_help(char* prog_path) {
  // Get the program file-name in prog_name (strip prog_path of the path)
  char* prog_name; // No new, it will point to somewhere inside prog_path
  if ((prog_name = strrchr(prog_path, '/'))) {
    prog_name++;
  }
  else {
    prog_name = prog_path;
  }

  printf("******************************************************************************\n");
  printf("*                                                                            *\n");
  printf("*                        aevol - Artificial Evolution                        *\n");
  printf("*                                                                            *\n");
  printf("* Aevol is a simulation platform that allows one to let populations of       *\n");
  printf("* digital organisms evolve in different conditions and study experimentally  *\n");
  printf("* the mechanisms responsible for the structuration of the genome and the     *\n");
  printf("* transcriptome.                                                             *\n");
  printf("*                                                                            *\n");
  printf("******************************************************************************\n");
  printf("\n");
  printf("%s:\n", prog_name);
  printf("\tReconstruct the lineage of a given individual from the tree files\n");
  printf("\n");
  printf("Usage : %s [-e TIMESTEP] [-I INDEX ] INPUT_JSON_FILE\n", prog_name);
  printf("\nOptions\n");
  printf("  -e, --end TIMESTEP\n");
  printf("\tspecify time t_end of the indiv whose lineage is to be reconstructed\n");
  printf("  -I, --index INDEX\n");
  printf("\tspecify the index of the indiv whose lineage is to be reconstructed\n");

}

void interpret_cmd_line_options(int argc, char* argv[]) {
  // Define allowed options
  const char * short_options = "I:e:";
  static struct option long_options[] = {
      {"index",     required_argument, nullptr, 'I'},
      {"end",       required_argument, nullptr, 'e'},
      {0, 0, 0, 0}
  };

  // Get actual values of the command-line options
  int option;
  while((option = getopt_long(argc, argv, short_options,
                              long_options, nullptr)) != -1) {
    switch(option) {
    case 'e' : {
      if (strcmp(optarg, "") == 0) {
        printf("%s: error: Option -e or --end : missing argument.\n",
               argv[0]);
        exit(EXIT_FAILURE);
      }
      t_end = std::stol(optarg);
      break;
    }
    case 'I' : {
      best = false;
      final_indiv_index = std::stoi(optarg);
      break;
    }
    default : {
      // An error message is printed in getopt_long, we just need to exit
      exit(EXIT_FAILURE);
    }
    }
  }

  for (int i = optind; i < argc; i++)
    if (input_file_name == nullptr)
      input_file_name = argv[i];
    else {
      std::cerr << "Please, specify only one input file" << std::endl;
      exit(EXIT_FAILURE);
    }

  if (input_file_name == nullptr) {
    std::cerr << "Please specify an input file" << std::endl;
    exit(EXIT_FAILURE);
  }

  if (t_end < 0) {
    fprintf(stderr, "Please specify an end time\n");
    exit(EXIT_FAILURE);
  }
}
