// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

// The input file is produced by the lineage post-treatment, please refer to it
// for e.g. the file format/content

// ============================================================================
//                                   Includes
// ============================================================================
#include "AeTime.h"
#include "Gaussian.h"
#include "json.hpp"
#include "Metadata.h"
#include "MutationDataAdapter.h"
#include "ReplicationReport.h"
#include "Stats_7.h"

#include <cerrno>
#include <cinttypes>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <err.h>
#include <getopt.h>
#include <list>
#include <string>
#include <sys/stat.h>
#include <zlib.h>
using namespace aevol;
using json = nlohmann::json;

// Helper functions
void interpret_cmd_line_options(int argc, char* argv[]);
void print_help(char* prog_path);

// Command-line option variables
static char* lineage_file_name = nullptr;
static char* json_file_name = nullptr;
static bool verbose = false;
static bool trace_mutations = false;


AbstractFuzzy_7* define_target(std::vector<Gaussian>& gaussians,
                               int sampling,
                               FuzzyFactory_7* factory) {
  auto* fuzzy = factory->create_fuzzy();
  for (int16_t i = 0; i <= sampling; i++) {
    Point new_point =
        Point(X_MIN + (double)i * (X_MAX - X_MIN) / (double)sampling, 0.0);
    for (const auto & g: gaussians) {
      new_point.y += g.compute_y(new_point.x);
    }
    fuzzy->add_point(new_point.x, new_point.y);
  }

  fuzzy->clip(AbstractFuzzy_7::min, Y_MIN);
  fuzzy->clip(AbstractFuzzy_7::max, Y_MAX);
  fuzzy->simplify();
  return fuzzy;
}

struct Parameters {
  int sampling;
  double w_max;
  double selection_pressure;
};

void from_json(const json& j, Parameters& p) {
  assert(j.at("individual").is_string());
  j.at("sampling").get_to(p.sampling);
  j.at("w_max").get_to(p.w_max);
  j.at("selection_pressure").get_to(p.selection_pressure);
}

int main(int argc, char* argv[]) {
  interpret_cmd_line_options(argc, argv);

  printf("input json file: %s\n", json_file_name);
  std::ifstream input_file(json_file_name);

  json inputs;
  input_file >> inputs;
  input_file.close();

  Parameters p = inputs.get<Parameters>();

  printf("\n"
         "WARNING : Parameter change during simulation is not managed in general.\n"
         "          Only changes in environmental target done with aevol_modify are handled.\n"
         "\n");

  // =======================
  //  Open the lineage file
  // =======================
  gzFile lineage_file = gzopen(lineage_file_name, "r");
  if (lineage_file == Z_NULL) {
    fprintf(stderr, "ERROR : Could not read the lineage file %s\n", lineage_file_name);
    delete [] lineage_file_name;
    exit(EXIT_FAILURE);
  }

  delete [] lineage_file_name;

  int64_t t0 = 0;
  int64_t t_end = 0;
  int32_t final_indiv_index = 0;
  int32_t final_indiv_rank  = 0;

  gzread(lineage_file, &t0, sizeof(t0));
  gzread(lineage_file, &t_end, sizeof(t_end));

  if (verbose) {
    printf("\n\n""===============================================================================\n");
    printf(" Statistics of the ancestors of indiv. %" PRId32
           " (rank %" PRId32 ") from time %" PRId64 " to %" PRId64 "\n",
           final_indiv_index, final_indiv_rank, t0, t_end);
    printf("================================================================================\n");
  }

  // =========================
  //  Open the output file(s)
  // =========================
  // Create missing directories
  int status;
  status = mkdir("stats/ancestor_stats/", 0755);
  if ((status == -1) && (errno != EEXIST)) {
    err(EXIT_FAILURE, "stats/ancestor_stats/");
  }

  // Open main output files (uses the Stats utility class)
  char prefix[255] = "stats/ancestor_stats";
  char postfix[255];
  snprintf(postfix, 255,
      "-b" TIMESTEP_FORMAT "-e" TIMESTEP_FORMAT "-i%" PRId32 "-r%" PRId32,
      t0, t_end, final_indiv_index, final_indiv_rank);

  Stats_7* stats_anc = new Stats_7(0, true, prefix ,true);
  // Optional additional outputs

  // Open optional output files
  FILE* fixed_mutations_file = nullptr;
  if (trace_mutations) {
    char fixed_mutations_file_name[255];
    snprintf(fixed_mutations_file_name, 255,
             "stats/ancestor_stats/fixedmut-b" TIMESTEP_FORMAT
             "-e" TIMESTEP_FORMAT "-i%" PRId32 "-r%" PRId32 ".out",
             t0, t_end, final_indiv_index, final_indiv_rank);
    fixed_mutations_file = fopen(fixed_mutations_file_name, "w");
    if (fixed_mutations_file == nullptr) {
      Utils::ExitWithUsrMsg(std::string("Could not create the output file ") +
                            fixed_mutations_file_name);
    }

    // Write the header
    fprintf(fixed_mutations_file, "# #################################################################\n");
    fprintf(fixed_mutations_file, "#  Mutations in the lineage of the best indiv at generation %" PRId64 "\n", t_end);
    fprintf(fixed_mutations_file, "# #################################################################\n");
    fprintf(fixed_mutations_file, "#  1.  Generation       (mut. occurred when producing the indiv. of this generation)\n");
    fprintf(fixed_mutations_file, "#  2.  Genetic unit     (which underwent the mutation, 0 = chromosome) \n");
    fprintf(fixed_mutations_file, "#  3.  Mutation type    (0: switch, 1: smallins, 2: smalldel, 3:dupl, 4: del, 5:trans, 6:inv, 7:insert, 8:ins_HT, 9:repl_HT) \n");
    fprintf(fixed_mutations_file, "#  4.  pos_0            (position for the small events, begin_segment for the rearrangements, begin_segment of the inserted segment for ins_HT, begin_segment of replaced segment for repl_HT) \n");
    fprintf(fixed_mutations_file, "#  5.  pos_1            (-1 for the small events, end_segment for the rearrangements, end_segment of the inserted segment for ins_HT, begin_segment of donor segment for repl_HT) \n");
    fprintf(fixed_mutations_file, "#  6.  pos_2            (reinsertion point for duplic., cutting point in segment for transloc., insertion point in the receiver for ins_HT, end_segment of the replaced segment for repl_HT, -1 for other events)\n");
    fprintf(fixed_mutations_file, "#  7.  pos_3            (reinsertion point for transloc., breakpoint in the donor for ins_HT, end_segment of the donor segment for repl_HT, -1 for other events)\n");
    fprintf(fixed_mutations_file, "#  8.  invert           (transloc, was the segment inverted (0/1)?, sense of insertion for ins_HT (0=DIRECT, 1=INDIRECT), sense of the donor segment for repl_HT (0=DIRECT, 1=INDIRECT),-1 for other events)\n");
    fprintf(fixed_mutations_file, "#  9.  align_score      (score that was needed for the rearrangement to occur, score of the first alignment for ins_HT and repl_HT)\n");
    fprintf(fixed_mutations_file, "#  10. align_score2     (score for the reinsertion for transloc, score of the second alignment for ins_HT and repl_HT)\n");
    fprintf(fixed_mutations_file, "#  11. seg_len          (segment length for rearrangement, donor segment length for ins_HT and repl_HT)\n");
    fprintf(fixed_mutations_file, "#  12. repl_seg_len     (replaced segment length for repl_HT, -1 for the others)\n");
    fprintf(fixed_mutations_file, "#  13. GU_length        (before the event)\n");
    fprintf(fixed_mutations_file, "#  14. Impact of the mutation on the metabolic error (negative value = smaller gap after = beneficial mutation) \n");
    fprintf(fixed_mutations_file, "#  15. Impact of the mutation on fitness (positive value = higher fitness after = beneficial mutation) \n");
    fprintf(fixed_mutations_file, "#  16. Selection coefficient of the mutation (s > 1 = higher fitness after = beneficial mutation) \n");
    fprintf(fixed_mutations_file, "####################################################################################################################\n");
    fprintf(fixed_mutations_file, "#\n");
    fprintf(fixed_mutations_file, "# Header for R\n");
    fprintf(fixed_mutations_file, "gener gen_unit mut_type pos_0 pos_1 pos_2 pos_3 invert align_score align_score_2 seg_len repl_seg_len GU_len impact_on_gap impact_on_fitness sel_coeff\n");

  }

  // Init target
  const auto& env = inputs.at("env");
  assert(env.is_array());
  std::vector<Gaussian> gaussians;
  for (const auto& g: env) {
    assert(g.is_array() && g.size() == 3);
    gaussians.emplace_back(g[0].get<double>(),
                           g[1].get<double>(),
                           g[2].get<double>());
  }

  auto* fuzzyFactory = new FuzzyFactory_7(VECTOR, p.sampling);
  auto* target = define_target(gaussians, p.sampling, fuzzyFactory);

  // ==================================================
  //  Prepare the initial ancestor and write its stats
  // ==================================================

  const auto& src_seq_str = inputs.at("individual").get<std::string>();
  Individual_7* indiv = new Individual_7(0.0);
  indiv->dna_ = new Dna_7(src_seq_str.length());
  indiv->dna_->set_indiv(src_seq_str.c_str(), src_seq_str.length(), indiv);

  indiv->evaluation_from_scratch(target, p.w_max, p.selection_pressure,
                                 fuzzyFactory);
  AeTime::set_time(t0);
  Stats_7::write_stat(*stats_anc, *indiv, AeTime::time(), true);
  indiv->dna_->reset_stat();

  if (verbose) {
    printf("Initial fitness     = %e\n", indiv->fitness);
    printf("Initial genome size = %" PRId32 "\n", indiv->dna_->length());
  }

  // ==========================================================================
  //  Replay the mutations to get the successive ancestors and analyze them
  // ==========================================================================
  ReplicationReport* rep = nullptr;
  int32_t index;
  char mut_descr_string[255];

  AeTime::plusplus();
  
  while (AeTime::time() <= t_end)
  {
    rep = new ReplicationReport(lineage_file);
    index = rep->id(); // who we are building...

    if (verbose)
        printf("Rebuilding ancestor at generation %" PRId64
            " (index %" PRId32 ")...", time(), index);

    // For each genetic unit, replay the replication (undergo all mutations)
    const auto& dnarep = rep->dna_replic_report();

    bool has_mutate = false;
    dnarep.iter_muts([&](const auto& mut) {
      int32_t unitlen_before = 0;
      double metabolic_error_before = 0.0;
      double fitness_before = 0.0;

      if (trace_mutations) {
        // Store initial values before the mutation
        metabolic_error_before = indiv->metaerror;
        fitness_before         = indiv->fitness;
        unitlen_before         = indiv->dna_->length();
      }

      // Apply mutation
      auto* mut_event = MutationDataAdapter::mutation_to_mutation_event(*mut);
      indiv->dna_->apply_mutation(*mut_event);
      delete mut_event;
      has_mutate = true;

      if (trace_mutations) {
        indiv->evaluate_after_mutation(target, p.w_max, p.selection_pressure,
                                       fuzzyFactory);

        // Compute the metabolic impact of the mutation
        double impact_on_metabolic_error =
            indiv->metaerror -
            metabolic_error_before;

        double impact_on_fitness = indiv->fitness - fitness_before;

        double selection_coefficient = indiv->fitness / fitness_before - 1.0;

        mut->generic_description_string(mut_descr_string);
        fprintf(fixed_mutations_file,
                "%" PRId64 " %" PRId32 " %s %" PRId32 " %.15e %.15e %.15e\n",
                AeTime::time(), 0, mut_descr_string, unitlen_before,
                impact_on_metabolic_error, impact_on_fitness,
                selection_coefficient);
      }
    });

    if (has_mutate) {
      // 3) All the mutations have been replayed and evaluated if they are any
      Stats_7::write_stat(*stats_anc, *indiv, AeTime::time(), true);
      indiv->dna_->reset_stat();
    }

    if (verbose) printf(" OK\n");

    delete rep;

    aevol::AeTime::plusplus();
  }

  gzclose(lineage_file);

  delete stats_anc;

  return EXIT_SUCCESS;
}

void interpret_cmd_line_options(int argc, char* argv[]) {
  // =====================
  //  Parse command line
  // =====================
  const char * short_options = "hvMj:";
  static struct option long_options[] = {
      {"help",                no_argument, NULL, 'h'},
      {"verbose",             no_argument, NULL, 'v'},
      {"trace-mutations",     no_argument, NULL, 'M'},
      {"json_file",     required_argument, NULL, 'j'},
      {0, 0, 0, 0}
  };

  int option;
  while((option = getopt_long(argc, argv, short_options,
                              long_options, nullptr)) != -1) {
    switch(option) {
      case 'h':
        print_help(argv[0]);
        exit(EXIT_SUCCESS);
      case 'v':
        verbose = true;
        break;
      case 'M':
        trace_mutations = true;
        break;
      case 'j':
        json_file_name = optarg;
        break ;
      default:
        fprintf(stderr, "option %c does not exist", option);
        // An error message is printed in getopt_long, we just need to exit
        exit(EXIT_FAILURE);
    }
  }

  // There should be only one remaining arg: the lineage file
  if (optind != argc - 1) {
    fprintf(stderr, "please specify a lineage file");
  }

  if (json_file_name == nullptr) {
    fprintf(stderr, "please specify a json file");
  }

  lineage_file_name = new char[strlen(argv[optind]) + 1];
  sprintf(lineage_file_name, "%s", argv[optind]);
}

void print_help(char* prog_path) {
  // Get the program file-name in prog_name (strip prog_path of the path)
  char* prog_name; // No new, it will point to somewhere inside prog_path
  if ((prog_name = strrchr(prog_path, '/'))) {
    prog_name++;
  }
  else {
    prog_name = prog_path;
  }

  printf("******************************************************************************\n");
  printf("*                                                                            *\n");
  printf("*                        aevol - Artificial Evolution                        *\n");
  printf("*                                                                            *\n");
  printf("* Aevol is a simulation platform that allows one to let populations of       *\n");
  printf("* digital organisms evolve in different conditions and study experimentally  *\n");
  printf("* the mechanisms responsible for the structuration of the genome and the     *\n");
  printf("* transcriptome.                                                             *\n");
  printf("*                                                                            *\n");
  printf("******************************************************************************\n");
  printf("\n");
  printf("%s: create an experiment with setup as specified in PARAM_FILE.\n",
  prog_name);
  printf("\n");
  printf("Usage : %s -h or --help\n", prog_name);
  printf("   Usage : %s LINEAGE_FILE [-Mv] -j JSON_FILE\n", prog_name);
  printf("\nOptions\n");
  printf("  -h, --help\n\tprint this help, then exit\n");
  printf("  -M, --trace-mutations\n");
  printf("\toutputs the fixed mutations (in a separate file)\n");
  printf("  -v, --verbose\n\tbe verbose\n");
}
