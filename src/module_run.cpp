#include "AeTime.h"
#include "Backup_handler.h"
#include "ExpManager_7.h"
#include "Gaussian.h"
#include "Individual_7.h"
#include "Dna_7.h"
#include "json.hpp"

#include <getopt.h>
#include <fstream>
#include <string>

#ifdef _OPENMP
  #include <omp.h>
#endif

using namespace aevol;
using json = nlohmann::json;

void init_prng(std::vector<std::unique_ptr<JumpingMT>>& reprod_prng,
               std::vector<std::shared_ptr<JumpingMT>>& mut_prng,
               int seed) {
  const int RANGE = 1000000;
  JumpingMT tmp_prng(seed);
  for (auto& prng: reprod_prng) {
    prng = std::make_unique<JumpingMT>(tmp_prng.random(RANGE));
  }
  for (auto& prng: mut_prng) {
    prng = std::make_shared<JumpingMT>(tmp_prng.random(RANGE));
  }
}

std::shared_ptr<Individual_7> setup_clonal_population(int seed,
                                                      const std::string& genome,
                                                      ExpManager_7& exp_m) {

  int src_seq_length = genome.length();
  const auto* src_seq = genome.data();

  auto src_indiv = std::make_shared<Individual_7>(0.0);
  src_indiv->dna_ = new Dna_7(src_seq_length);
  src_indiv->dna_->set_indiv(src_seq, src_seq_length, src_indiv.get());

  for (auto& indiv_location: exp_m.previous_individuals) {
    indiv_location = src_indiv;
  }

  init_prng(exp_m.reprod_prng_, exp_m.mut_prng_, seed);

  return src_indiv;
}

AbstractFuzzy_7* define_target(std::vector<Gaussian>& gaussians,
                               int sampling,
                               FuzzyFactory_7* factory) {
  auto* fuzzy = factory->create_fuzzy();
  for (int16_t i = 0; i <= sampling; i++) {
    Point new_point =
        Point(X_MIN + (double)i * (X_MAX - X_MIN) / (double)sampling, 0.0);
    for (const auto & g: gaussians) {
      new_point.y += g.compute_y(new_point.x);
    }
    fuzzy->add_point(new_point.x, new_point.y);
  }

  fuzzy->clip(AbstractFuzzy_7::min, Y_MIN);
  fuzzy->clip(AbstractFuzzy_7::max, Y_MAX);
  fuzzy->simplify();
  return fuzzy;
}

struct Parameters {
  int seed;
  int sampling;
  double w_max;
  double selection_pressure;
  int backup_step;
  int tree_step;
  int width;
  int height;
};

void from_json(const json& j, Parameters& p) {
  assert(j.at("individual").is_string());
  j.at("mutation_parameters");
  j.at("seed").get_to(p.seed);
  j.at("sampling").get_to(p.sampling);
  j.at("w_max").get_to(p.w_max);
  j.at("selection_pressure").get_to(p.selection_pressure);
  j.at("backup_step").get_to(p.backup_step);
  j.at("tree_step").get_to(p.tree_step);
  j.at("grid_dimension")[0].get_to(p.width);
  j.at("grid_dimension")[1].get_to(p.height);
}

namespace aevol {
void from_json(const json& j, MutationParameters& m) {
  j.at("point_mutation").get_to(m.point_mutation_rate_);
  j.at("small_insertion").get_to(m.small_insertion_rate_);
  j.at("small_deletion").get_to(m.small_deletion_rate_);
  j.at("max_indel_size").get_to(m.max_indel_size_);
  j.at("duplication").get_to(m.duplication_rate_);
  j.at("deletion").get_to(m.deletion_rate_);
  j.at("translocation").get_to(m.translocation_rate_);
  j.at("inversion").get_to(m.inversion_rate_);
  m.max_genome_length_ = 10000000;
  m.min_genome_length_ = 1;
}
}

void interprete_cmd_line(int argc, char** argv,
                         long& t_begin, long& t_end,
                         char*& input_file_name) {
#ifdef _OPENMP
  omp_set_num_threads(1);
#endif
  int c;
  while ((c = getopt(argc, argv, "b:e:p:")) != -1) {
    switch (c) {
    case 'b':
      t_begin = std::stol(optarg);
      break;
    case 'e':
      t_end = std::stol(optarg);
      break;
    case 'p':
#ifdef _OPENMP
      if (std::stoi(optarg) > 0) {
        omp_set_num_threads(atoi(optarg));
      }
#endif
      break;
    default:
      fprintf(stderr, "Option does not exist\n");
      exit(EXIT_FAILURE);
    }
  }

  for (int i = optind; i < argc; i++)
    if (input_file_name == nullptr)
      input_file_name = argv[i];
    else {
      std::cerr << "Please, specify only one input file" << std::endl;
      exit(EXIT_FAILURE);
    }

  if (input_file_name == nullptr) {
    std::cerr << "Please specify an input file" << std::endl;
    exit(EXIT_FAILURE);
  }

  if (t_end < 0) {
    fprintf(stderr, "Please specify an end time\n");
    exit(EXIT_FAILURE);
  }

  if (t_begin < 0)
    t_begin = 0;
}

int main(int argc, char** argv) {
  char* input_file_name = nullptr;
  long t_end = -1;
  long t_begin = -1;

  interprete_cmd_line(argc, argv, t_begin, t_end, input_file_name);

  std::ifstream input_file(input_file_name);
  AeTime::set_time(t_begin);

  json inputs;
  input_file >> inputs;
  input_file.close();

  Parameters p = inputs.get<Parameters>();
  MutationParameters mp =
      inputs.at("mutation_parameters").get<MutationParameters>();

  const auto& env = inputs.at("env");
  assert(env.is_array());

  std::vector<Gaussian> gaussians;
  for (const auto& g: env) {
    assert(g.is_array() && g.size() == 3);
    gaussians.emplace_back(g[0].get<double>(),
                           g[1].get<double>(),
                           g[2].get<double>());
  }

  auto* fuzzyFactory = new FuzzyFactory_7(VECTOR, p.sampling);
  auto* target = define_target(gaussians, p.sampling, fuzzyFactory);

  printf("target area: %f\n", target->get_geometric_area());

  constexpr int DEFAULT_SCOPE = 3;

  std::unique_ptr<Selector> selector;
  if (inputs.contains("selection")) {
    assert(inputs.at("selection").is_string());
    const auto& param = inputs.at("selection").get<std::string>();
    if (param == "global") {
      selector = std::make_unique<GlobalSelector>(p.height * p.width);
      printf("Global selection will by applied\n");
    } else if (param == "local") {
      selector = std::make_unique<LocalSelector>(DEFAULT_SCOPE, DEFAULT_SCOPE,
                                                 p.height, p.width);
      printf("Local (3x3) selection will by applied\n");
    } else {
      fprintf(stderr, "Error: selection option not valid\n");
      exit(EXIT_FAILURE);
    }
  } else {
    selector = std::make_unique<LocalSelector>(DEFAULT_SCOPE, DEFAULT_SCOPE,
                                               p.height, p.width);
    printf("Local (3x3) selection will by applied\n");
  }

  ExpManager_7 exp_m(p.width, p.height, p.backup_step, p.tree_step, mp,
                     fuzzyFactory, target, std::move(selector));

  if (AeTime::time() > 0) {
    printf("running on Backup\n");
    auto unique_indivs =
        bu::read_backup(AeTime::time(),
                        exp_m.previous_individuals,
                        exp_m.reprod_prng_, exp_m.mut_prng_);
    for (auto& indiv: unique_indivs) {
      indiv->evaluation_from_scratch(target, p.w_max, p.selection_pressure,
                                     fuzzyFactory);
    }
  } else {
    const auto& src_seq_string = inputs.at("individual").get<std::string>();
    auto src_indiv = setup_clonal_population(p.seed, src_seq_string, exp_m);

    src_indiv->evaluation_from_scratch(target, p.w_max, p.selection_pressure,
                                       fuzzyFactory);
    printf("src_indiv fitness: %e\n", src_indiv->fitness);
    printf("src_indiv meta_error: %e\n", src_indiv->metaerror);
  }

  bool the_end = false;
#pragma omp parallel
  while (not the_end) {
#pragma omp single
    AeTime::plusplus();

    exp_m.run_a_step(p.w_max, p.selection_pressure);

#pragma omp single
    {
      const auto& sample_indiv = exp_m.previous_individuals[0];
      printf("Generation %ld\n", AeTime::time());
      printf("Fitness of indiv at location 0 is %e (metaerror: %e)\n",
             sample_indiv->fitness, sample_indiv->metaerror);
      if (AeTime::time() % p.backup_step == 0) {
        printf("Writing backup !!\n");
        bu::write_backup(AeTime::time(),
                         exp_m.previous_individuals,
                         exp_m.reprod_prng_, exp_m.mut_prng_);
      }
      the_end = (AeTime::time() == t_end);
    }
  }

  printf("End of simulation !\n");

  return 0;
}
