#include "MutationFactorySswm.h"

#include "AbstractFuzzy_7.h"
#include "AeTime.h"
#include "DnaMutator.h"
#include "Individual_7.h"
#include "JumpingMT.h"
#include "Metadata.h"
#include "MutationDataAdapter.h"
#include "Promoter.h"
#include "Protein_7.h"
#include "Rna_7.h"

#include <algorithm>
#include <chrono>
#include <err.h>
#include <fstream>
#include <sys/stat.h>
#include <utility>

MutationFactorySswm::MutationFactorySswm(int seed,
                           MutationParametersSswm mut_parameters)
  : mp_sswm(mut_parameters), prng_nb(seed) {}


bool MutationFactorySswm::is_inversion() {
  auto* probs = new double[2];
  probs[0] = mp_sswm.inversion_rate_; // prob[0] = probability of inversion
  probs[1] = 1-probs[0];
  int nb = prng_nb.roulette_random(probs, 2);
  return nb == 0; // If nb == 0, it 's an inversion
}

aevol::MutationEvent MutationFactorySswm::do_mutation(int dna_lenght) {
  bool is_inversion_bool = is_inversion();
//  _____ If inversion
  if (is_inversion_bool) {
    int32_t pos_1, pos_2;
    pos_1 = prng_nb.random(dna_lenght);
    pos_2 = prng_nb.random(dna_lenght);

    while (pos_2 == pos_1)
    {
      pos_2 = prng_nb.random(dna_lenght);
    }

    // if (pos_1 == pos_2) return nullptr; // Invert everything <=> Invert nothing!

    // Invert the segment that don't contain OriC
    if (pos_1 > pos_2) aevol::Utils::exchange(pos_1, pos_2);

    int segment_length = pos_2 - pos_1;
    auto mevent = aevol::MutationEvent();
    mevent.inversion(pos_1, pos_2, segment_length);
//    printf("- Its a inversion\n");
    return mevent;
  }
//  _____If not inversion
  else {
    int32_t pos = prng_nb.random(dna_lenght);
    auto mevent = aevol::MutationEvent();
    mevent.switch_pos(pos);
//    printf("Its a ponctual mutation\n");
    return mevent;
  }
}
