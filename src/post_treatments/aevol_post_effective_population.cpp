// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

// =================================================================
//                              Libraries
// =================================================================
#include <algorithm>
#include <errno.h>
#include <fstream>
#include <getopt.h>
#include <inttypes.h>
#include <iostream>
#include <list>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unordered_map>
#include <vector>
#include <zlib.h>
// =================================================================
//                            Project Files
// =================================================================
#include "aevol.h"

using namespace aevol;

// =================================================================
//                         Function declarations
// =================================================================
void print_help(char* prog_path);

int main(int argc, char** argv) {
  printf("\n  WARNING : Parameters' change in the middle of a simulation is "
         "not managed.\n");

  // =====================
  //  Parse command line
  // =====================

  // Default values
  bool verbose = false;

  const char* short_options           = "hVv::";
  static struct option long_options[] = {{"help", no_argument, NULL, 'h'},
                                         {"version", no_argument, NULL, 'V'},
                                         {"verbose", no_argument, NULL, 'v'},
                                         {0, 0, 0, 0}};

  int option;
  while ((option = getopt_long(argc, argv, short_options, long_options,
                               NULL)) != -1) {
    switch (option) {
    case 'h': {
      print_help(argv[0]);
      exit(EXIT_SUCCESS);
    }
    case 'V': {
      Utils::PrintAevolVersion();
      exit(EXIT_SUCCESS);
    }
    case 'v':
      verbose = true;
      break;
      //case 'n' : check_genome = NO_CHECK;           break;
      //case 'c' : check_genome = FULL_CHECK;         break;
      //case 'b' : t0  = atol(optarg);                break;
      //case 'i' : final_indiv_index  = atol(optarg); break;
      //case 'r' : final_indiv_rank  = atol(optarg);  break;
    }
  }

  // There should be only one remaining arg: the lineage file
  if (optind != argc - 1) {
    Utils::ExitWithUsrMsg("please specify a coalescence file");
  }

  char* coalescence_file_name = new char[strlen(argv[optind]) + 1];
  sprintf(coalescence_file_name, "%s", argv[optind]);

  // =======================
  //  Open the coalescence file
  // =======================
  std::ifstream coalescence_file(coalescence_file_name, std::ifstream::in);
  std::ofstream effective_population_file;
  effective_population_file.open("effective_population.csv",
                                 std::ofstream::trunc);
  effective_population_file << "Generation,"
                            << "Effective Population" << std::endl;

  //this is just the column names
  std::string line;
  std::getline(coalescence_file, line);

  int N = 256;
  float eff_pop;
  //using the scaling constant for a haploid wright fisher population.
  float scaling_constant = 2 * N;

  //Writes the effective population to a file with the generation
  while (std::getline(coalescence_file, line)) {
    std::stringstream ss(line);
    std::string generation, coal_time;

    std::getline(ss, generation, ',');
    std::getline(ss, coal_time, ',');
    eff_pop = (stoi(coal_time) / scaling_constant) * N;
    effective_population_file << generation << "," << eff_pop << std::endl;
  }

  coalescence_file.close();

  effective_population_file.flush();
  effective_population_file.close();

  free(coalescence_file_name);

  exit(EXIT_SUCCESS);
}

/*!
  \brief

*/
void print_help(char* prog_path) {
  // default values :
  // begin_gener = 0
  // indiv  = best individual at generation end_gener

  // there must be a genome backup file for begin_gener

  // not relevant if crossover

  printf("\n");
  printf("*********************** aevol - Artificial Evolution "
         "******************* \n");
  printf("*                                                                    "
         "  * \n");
  printf("*                      Lineage post-treatment program                "
         "  * \n");
  printf("*                                                                    "
         "  * \n");
  printf("*********************************************************************"
         "*** \n");
  printf("\n\n");
  printf("This program is Free Software. No Warranty.\n");
  printf("Copyright (C) 2009  LIRIS.\n");
  printf("\n");
#ifdef __REGUL
  printf("Usage : rlineage -h\n");
  printf("or :    rlineage [-vn] [-i index | -r rank] [-b gener1] -e end_gener "
         "\n");
#else
  printf("Usage : lineage -h\n");
  printf(
      "or :    lineage [-vn] [-i index | -r rank] [-b gener1] -e end_gener \n");
#endif
  printf("\n");
#ifdef __REGUL
  printf("This program retrieves the ancestral lineage of an individual and "
         "writes \n");
  printf("it in an output file called lineage.rae. Specifically, it retrieves "
         "the \n");
  printf(
      "lineage of the individual of end_gener whose index is index, going \n");
  printf("back in time up to gener1. This program requires at least one "
         "population backup\n");
  printf("file (for the generation gener1), one environment backup file (for "
         "the generation gener1)\n");
  printf("and all tree files for generations gener1 to end_gener.\n");
#else
  printf("This program retrieves the ancestral lineage of an individual and "
         "writes \n");
  printf("it in an output file called lineage.ae. Specifically, it retrieves "
         "the \n");
  printf(
      "lineage of the individual of end_gener whose index is index, going \n");
  printf("back in time up to gener1. This program requires at least one "
         "population backup\n");
  printf("file (for the generation gener1), one environment backup file (for "
         "the generation gener1)\n");
  printf("and all tree files for generations gener1 to end_gener.\n");
#endif
  printf("\n");
  printf("WARNING: This program should not be used for simulations run with "
         "lateral\n");
  printf("transfer. When an individual has more than one parent, the notion of "
         "lineage\n");
  printf("used here is not relevant.\n");
  printf("\n");
  printf("\t-h or --help    : Display this help.\n");
  printf("\n");
  printf("\t-v or --verbose : Be verbose, listing generations as they are \n");
  printf("\t                  treated.\n");
  printf("\n");
  printf(
      "\t-n or --nocheck    : Disable genome sequence checking. Makes the \n");
  printf(
      "\t                       program faster, but it is not recommended. \n");
  printf(
      "\t                       It is better to let the program check that \n");
  printf("\t                       when we rebuild the genomes of the "
         "ancestors\n");
  printf("\t                       from the lineage file, we get the same "
         "sequences\n");
  printf("\t                       as those stored in the backup files.\n");
  printf("\n");
  printf("\t-c or --fullcheck  : Will perform the genome checks every "
         "<BACKUP_STEP>\n");
  printf("\t                       generations. Default behaviour is lighter "
         "as it\n");
  printf("\t                       only performs these checks at the ending "
         "generation.\n");
  printf("\n");
  printf("\t-i index or --index index : \n");
  printf("\t                  Retrieve the lineage of the individual whose\n");
  printf("\t                  index is index. The index must be comprised \n");
  printf("\t                  between 0 and N-1, with N the size of the \n");
  printf(
      "\t                  population at the ending generation. If neither\n");
  printf("\t                  index nor rank are specified, the program "
         "computes \n");
  printf("\t                  the lineage of the best individual of the ending "
         "\n");
  printf("\t                  generation.\n");
  printf("\n");
  printf("\t-r rank or --rank rank : \n");
  printf("\t                  Retrieve the lineage of the individual whose\n");
  printf("\t                  rank is rank. The rank must be comprised \n");
  printf("\t                  between 1 and N, with N the size of the \n");
  printf(
      "\t                  population at the endind generation. If neither\n");
  printf("\t                  index nor rank are specified, the program "
         "computes \n");
  printf("\t                  the lineage of the best individual of the ending "
         "\n");
  printf("\t                  generation.\n");
  printf("\n");
  printf("\t-b gener1 or --begin gener1 : \n");
  printf("\t                  Retrieve the lineage up to generation gener1.\n");
  printf("\t                  There must be a genome backup file for this\n");
  printf("\t                  generation. If not specified, the program \n");
  printf("\t                  retrieves the lineage up to generation 0.\n");
  printf("\n");
  printf("\t-e end_gener or --end end_gener : \n");
  printf("\t                  Retrieve the lineage of the individual of "
         "end_gener \n");
  printf("\t                  (default: that contained in file last_gener.txt, "
         "if any)\n");
  printf("\n");
}
