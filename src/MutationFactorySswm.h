//
// Created by yanissb on 22/06/22.
//
#include "FuzzyFactory_7.h"
#include "DnaMutator.h"
#include "Tree.h"
#include "Stats_7.h"
#include "Individual_7.h"


#ifndef AEVOL_MUTATIONSSWM_H
#define AEVOL_MUTATIONSSWM_H

struct MutationParametersSswm {
  double inversion_rate_;
//  int32_t min_genome_length_;
//  int32_t max_genome_length_;
};

class MutationFactorySswm {
  bool is_inversion();

  const MutationParametersSswm mp_sswm;
  aevol::JumpingMT prng_nb;
 public:
  MutationFactorySswm(int seed, MutationParametersSswm mut_parameters);

  aevol::MutationEvent do_mutation(int dna_lenght);

};

#endif  //AEVOL_MUTATIONSSWM_H
