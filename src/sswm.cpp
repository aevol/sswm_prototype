// _________
#include "Dna_7.h"
#include "Gaussian.h"
#include "Individual_7.h"
#include "json.hpp"
#include "JumpingMT.h"
#include "MutationFactorySswm.h"
#include "Metadata.h"

#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

using namespace aevol;
using json = nlohmann::json;


#ifdef BASE_4
  #define NB_BASE 4
constexpr double ERROR_MARGIN = 1E4;
#else
  #define NB_BASE 2
constexpr double ERROR_MARGIN = 1E6;
#endif

AbstractFuzzy_7* define_target(std::vector<Gaussian>& gaussians,
                               int sampling,
                               FuzzyFactory_7& factory) {
  auto* fuzzy = factory.create_fuzzy();
  for (int16_t i = 0; i <= sampling; i++) {
    Point new_point =
        Point(X_MIN + (double)i * (X_MAX - X_MIN) / (double)sampling, 0.0);
    for (const auto & g: gaussians) {
      new_point.y += g.compute_y(new_point.x);
    }
    fuzzy->add_point(new_point.x, new_point.y);
  }

  fuzzy->clip(AbstractFuzzy_7::min, Y_MIN);
  fuzzy->clip(AbstractFuzzy_7::max, Y_MAX);
  fuzzy->simplify();
  return fuzzy;
}

int fun_prot_counter(aevol::Individual_7* indiv) {
  auto prot_list = indiv->metadata_->proteins_;
  int nb_fun_prot = 0;
  for (const auto& seq : prot_list) {
    if (seq->is_functional) {
      nb_fun_prot++;
    }
  }
  return nb_fun_prot;
}

struct Parameters {
  int seed;
  int sampling;
  double w_max;
  double selection_pressure;
  int begin_size;
};

void from_json(const json& j, Parameters& p) {
  assert(j.at("individual").is_string());
  j.at("mutation_parameters");
  j.at("seed").get_to(p.seed);
  j.at("sampling").get_to(p.sampling);
  j.at("w_max").get_to(p.w_max);
  j.at("selection_pressure").get_to(p.selection_pressure);
}

int main(int argc, char* argv[]) { // USAGE: program GENE_JSON_FILE MUTATION_RATE NB_GENERATION NEUTRAL_MUTATION_BOOL HIKE_NUMBER
  if (argc != 6 and argc != 5 ) {
    printf("!!!\nPlease don't forget to write the json file then the"
           " inversion rate, the number of generation"
           " and to chose if neutral mutation are selected\n!!!\n");
    exit(EXIT_FAILURE);
  }
  else if (std::stof(argv[2]) > 1 or std::stof(argv[2]) < 0) {
    printf("!!!\nPlease select a inversion rate between 0 and 1\n!!!\n");
    exit(EXIT_FAILURE); } // Todo : verif que ça soit bien convertissable
  else if (std::stol(argv[3]) <= 0) {
    printf("!!!\nPlease select positive number of generation\n!!!\n");
    exit(EXIT_FAILURE); }
  char* input_file_name = argv[1];
  std::ifstream input_file(input_file_name);
  const float inversion_rate = std::stof(argv[2]);
  const long nb_gen = std::stol(argv[3]);
  const std::string if_eq = argv[4];
  std::string hikes;
  if (argc == 6) {
    hikes = argv[5];
  }

  json inputs;
  input_file >> inputs;
  input_file.close();

  auto p = inputs.get<Parameters>();
  MutationParametersSswm mp{};
  mp.inversion_rate_ = inversion_rate;
#ifdef BASE_4
  Dna_7::polya_length = inputs.at("polya_length").get<int8_t>();
#endif

  const auto& env = inputs.at("env");
  assert(env.is_array());

  std::vector<Gaussian> gaussians;
  for (const auto& g: env) {
    assert(g.is_array() && g.size() == 3);
    gaussians.emplace_back(g[0].get<double>(),
                           g[1].get<double>(),
                           g[2].get<double>());
  }
  FuzzyFactory_7 fuzzyFactory(VECTOR, p.sampling);
  auto* target = define_target(gaussians, p.sampling, fuzzyFactory);

  const auto& seq = inputs.at("individual").get<std::string>();
  int src_seq_length = seq.length();
  const auto* src_seq = seq.data();

  Individual_7* individual = nullptr;
  individual = new Individual_7(0.0);
  individual->dna_ = new Dna_7(src_seq_length);
  individual->dna_->set_indiv(src_seq, src_seq_length, individual);
  individual->evaluation_from_scratch(target, p.w_max, p.selection_pressure,
                                      &fuzzyFactory);
  Individual_7* new_indiv = nullptr;

  auto* fuzzyFac = &fuzzyFactory;
  const int dna_length = individual->dna_->length();
  long good_mut = 0;
  long neutral_mut = 0;
  int new_nb_prot = 0;
  int nb_prot_kept = 0;


  std::string underscore = "_";
  std::string base_evol_file = "sswm_evol_result_";
  std::string nbgen_result_file = std::to_string(nb_gen);
  std::string who_result_file = input_file_name;
  std::string prob_result_file = std::to_string(inversion_rate);
  std::string extension = ".csv";
  std::string result_evol_file = base_evol_file +
                                 nbgen_result_file +
                                 who_result_file +
                                 underscore +
                                 prob_result_file +
                                 underscore +
                                 if_eq +
                                 underscore +
                                 hikes +
                                 underscore +
                                 extension;
  std::ofstream output_evol_file(result_evol_file);
  output_evol_file << " nb_gen,fitness_kept,new_fitness,mutation,nb_prot_kept,new_nb_prot" << std::endl;


  MutationFactorySswm mutationFactorySswm(p.seed, mp);


  if (not output_evol_file.is_open()) {
    fprintf(stderr, "Error, cannot open file %s\n", result_evol_file.c_str()); // utilisation de `fprintf` parmet d'écrire spécifiquement dans le stderr. C'est explicitement une erreur
    exit(EXIT_FAILURE);
  }
  for (int i = 1; i <= nb_gen; i++) {
    auto mutationEvent = mutationFactorySswm.do_mutation(dna_length);
    new_indiv = new aevol::Individual_7(individual);
    new_indiv->dna_->apply_mutation(mutationEvent);
    new_indiv->evaluate_after_mutation(target, p.w_max,
                                       p.selection_pressure, fuzzyFac);


    if (individual->fitness < new_indiv->fitness) {
      delete individual;
      individual = new_indiv;
      nb_prot_kept = fun_prot_counter(individual);
      new_nb_prot = nb_prot_kept;
      output_evol_file << i << "," << individual->fitness << ","
                       << new_indiv->fitness << "," << mutationEvent.type()
                       << "," << nb_prot_kept << "," << new_nb_prot
                       << std::endl;
      good_mut++;
    }
    else if (individual->fitness == new_indiv->fitness and if_eq == "y") {
      delete individual;
      individual = new_indiv;
      nb_prot_kept = fun_prot_counter(individual);
      new_nb_prot = nb_prot_kept;
      output_evol_file << i << "," << individual->fitness << ","
                       << new_indiv->fitness << "," << mutationEvent.type()
                       << "," << nb_prot_kept << "," << new_nb_prot
                       << std::endl;
      neutral_mut++;
    }
    else {
      new_nb_prot = fun_prot_counter(new_indiv);
      nb_prot_kept = fun_prot_counter(individual);
      output_evol_file << i << "," << individual->fitness << ","
                       << new_indiv->fitness << "," << mutationEvent.type()
                       << "," << nb_prot_kept << "," << new_nb_prot
                       << std::endl;
      delete new_indiv;
      new_indiv = nullptr;
    }
    if (i % 5000 == 0) {
      printf("---   Generation %d done   ---\n", i);
    }
  }
  std::string result(individual->dna_->data());
  inputs["individual"] = result;

  std::string base_result_file = "sswm_result_";
  std::string result_file = base_result_file +
                            nbgen_result_file +
                            underscore +
                            prob_result_file +
                            underscore +
                            if_eq +
                            underscore +
                            hikes +
                            underscore +
                            who_result_file;
  std::ofstream output_file(result_file);
  output_file << std::setw(2) << inputs << std::endl;

  delete individual;
  printf("There are %ld positive(s) mutations(s), %ld neutral(s) mutation(s) and %ld negative(s) mutation(s)",good_mut, neutral_mut,nb_gen - good_mut - neutral_mut);

  return 0;
}