#include "Dna_7.h"
#include "Gaussian.h"
#include "Individual_7.h"
#include "json.hpp"
#include "JumpingMT.h"

#include <fstream>
#include <iomanip>
#include <string>

using namespace aevol;
using json = nlohmann::json;

#ifdef BASE_4
  #define NB_BASE 4
  constexpr double ERROR_MARGIN = 1E4;
#else
  #define NB_BASE 2
  constexpr double ERROR_MARGIN = 1E6;
#endif

template<int N_BASE>
void generate_random_sequence(char* seq, int size, JumpingMT& prng) {
  for (int i = 0; i < size; ++i) {
    seq[i] = '0' + prng.random(N_BASE);
  }
  seq[size] = '\0';
}

AbstractFuzzy_7* define_target(std::vector<Gaussian>& gaussians,
                               int sampling,
                               FuzzyFactory_7& factory) {
  auto* fuzzy = factory.create_fuzzy();
  for (int16_t i = 0; i <= sampling; i++) {
    Point new_point =
        Point(X_MIN + (double)i * (X_MAX - X_MIN) / (double)sampling, 0.0);
    for (const auto & g: gaussians) {
      new_point.y += g.compute_y(new_point.x);
    }
    fuzzy->add_point(new_point.x, new_point.y);
  }

  fuzzy->clip(AbstractFuzzy_7::min, Y_MIN);
  fuzzy->clip(AbstractFuzzy_7::max, Y_MAX);
  fuzzy->simplify();
  return fuzzy;
}

struct Parameters {
  int seed;
  int begin_size;
  int sampling;
  double w_max;
  double selection_pressure;
};

void from_json(const json& j, Parameters& p) {
  j.at("seed").get_to(p.seed);
  j.at("begin_size").get_to(p.begin_size);
  j.at("sampling").get_to(p.sampling);
  j.at("w_max").get_to(p.w_max);
  j.at("selection_pressure").get_to(p.selection_pressure);
}


int main(int argc, char* argv[]) {
  if (argc != 2)
    exit(EXIT_FAILURE);
  char* input_file_name = argv[1];
  std::ifstream input_file(input_file_name);

  json inputs;
  input_file >> inputs;
  input_file.close();

  Parameters p = inputs.get<Parameters>();
#ifdef BASE_4
  Dna_7::polya_length = inputs.at("polya_length").get<int8_t>();
#endif

  const auto& env = inputs.at("env");
  assert(env.is_array());

  std::vector<Gaussian> gaussians;
  for (const auto& g: env) {
    assert(g.is_array() && g.size() == 3);
    gaussians.emplace_back(g[0].get<double>(),
                           g[1].get<double>(),
                           g[2].get<double>());
  }
  FuzzyFactory_7 fuzzyFactory(VECTOR, p.sampling);
  auto* target = define_target(gaussians, p.sampling, fuzzyFactory);

  Individual_7* individual = nullptr;
  JumpingMT prng(p.seed);

  char* seq = new char[p.begin_size + 1];
  double r_compare;
  int nb_tries = 0;
  do {
    delete individual;
    individual = new Individual_7(0.0);
    individual->dna_ = new Dna_7(p.begin_size);
    generate_random_sequence<NB_BASE>(seq, p.begin_size, prng);
    individual->dna_->set_indiv(seq, p.begin_size, individual);

    individual->evaluation_from_scratch(target, p.w_max, p.selection_pressure,
                                       &fuzzyFactory);
    r_compare = round((individual->metaerror - target->get_geometric_area()) *
                      ERROR_MARGIN) / ERROR_MARGIN;
    nb_tries++;
    if (nb_tries % 1000 == 0) {
      printf("%d genomes tried\n", nb_tries);
    }
  } while (r_compare >= 0.0);

  printf("A worthy enough individual have been found !!\n"
         "Here is its genome:\n");
  printf("%s\n", individual->dna_->data());
  printf("Fitness is %e\n", individual->fitness);
  printf("Meta_error is %e\n", individual->metaerror);

  printf("target area: %f\n", target->get_geometric_area());

  std::string result(individual->dna_->data());
  inputs["individual"] = result;

  std::ofstream output_file("result.json");
  output_file << std::setw(2) << inputs << std::endl;

  delete target;
  delete[] seq;
  return 0;
}
